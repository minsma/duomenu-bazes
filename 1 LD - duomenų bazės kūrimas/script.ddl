#@(#) script.ddl

DROP TABLE IF EXISTS VARZYBOS_STADIONAI;
DROP TABLE IF EXISTS SIRGALIAI_VARZYBOS;
DROP TABLE IF EXISTS KORTELES;
DROP TABLE IF EXISTS IVARCIAI;
DROP TABLE IF EXISTS ZAIDEJAI;
DROP TABLE IF EXISTS VARZYBOS;
DROP TABLE IF EXISTS BILIETAI;
DROP TABLE IF EXISTS STADIONAI;
DROP TABLE IF EXISTS SIRGALIAI;
DROP TABLE IF EXISTS KOMANDOS;
DROP TABLE IF EXISTS stiprioji_koja;
DROP TABLE IF EXISTS pozicija;
DROP TABLE IF EXISTS TRENERIAI;
DROP TABLE IF EXISTS TEISEJAI;
DROP TABLE IF EXISTS ADRESAI;
CREATE TABLE ADRESAI
(
	id int NOT NULL AUTO_INCREMENT,
	salis varchar (255) NOT NULL,
	miestas varchar (255) NOT NULL,
	gatve varchar (255) NOT NULL,
	pastato_nr int NOT NULL,
	zip_kodas varchar (255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE TEISEJAI
(
	id int NOT NULL AUTO_INCREMENT,
	vardas varchar (255) NOT NULL,
	pavarde varchar (255) NOT NULL,
	gimimo_data date NOT NULL,
	tautybe varchar (255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE TRENERIAI
(
	id int NOT NULL AUTO_INCREMENT,
	vardas varchar (255) NOT NULL,
	pavarde varchar (255) NOT NULL,
	gimimo_data date NOT NULL,
	tautybe varchar (255) NOT NULL,
	ugis double NOT NULL,
	atlyginimas double NOT NULL,
	kontrakto_pabaiga date NOT NULL,
	rinkos_verte double NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE pozicija
(
	id_pozicija integer,
	name char (11) NOT NULL,
	PRIMARY KEY(id_pozicija)
);
INSERT INTO pozicija(id_pozicija, name) VALUES(1, 'puolejas');
INSERT INTO pozicija(id_pozicija, name) VALUES(2, 'saugas');
INSERT INTO pozicija(id_pozicija, name) VALUES(3, 'gynejas');
INSERT INTO pozicija(id_pozicija, name) VALUES(4, 'vartininkas');

CREATE TABLE stiprioji_koja
(
	id_stiprioji_koja integer,
	name char (6) NOT NULL,
	PRIMARY KEY(id_stiprioji_koja)
);
INSERT INTO stiprioji_koja(id_stiprioji_koja, name) VALUES(1, 'kaire');
INSERT INTO stiprioji_koja(id_stiprioji_koja, name) VALUES(2, 'desine');
INSERT INTO stiprioji_koja(id_stiprioji_koja, name) VALUES(3, 'abi');

CREATE TABLE KOMANDOS
(
	id int NOT NULL AUTO_INCREMENT,
	pavadinimas varchar (255) NOT NULL,
	salis varchar (255) NOT NULL,
	miestas varchar (255) NOT NULL,
	ikurimo_data date NOT NULL,
	svetaine varchar (255) NOT NULL,
	biudzetas double NOT NULL,
	fk_TRENERIS int NOT NULL,
	PRIMARY KEY(id),
	UNIQUE(fk_TRENERIS),
	CONSTRAINT treniruoja FOREIGN KEY(fk_TRENERIS) REFERENCES TRENERIAI (id)
);

CREATE TABLE SIRGALIAI
(
	id int NOT NULL AUTO_INCREMENT,
	vardas varchar (255) NOT NULL,
	pavarde varchar (255) NOT NULL,
	telefono_numeris varchar (255) NOT NULL,
	elektroninis_pastas varchar (255) NOT NULL,
	gimimo_data date NOT NULL,
	fk_ADRESAS int NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT gyvena FOREIGN KEY(fk_ADRESAS) REFERENCES ADRESAI (id)
);

CREATE TABLE STADIONAI
(
	id int NOT NULL AUTO_INCREMENT,
	pavadinimas varchar (255) NOT NULL,
	telefono_numeris varchar (255) NOT NULL,
	elektroninis_pastas varchar (255) NOT NULL,
	talpa int NOT NULL,
	fk_ADRESAS int NOT NULL,
	PRIMARY KEY(id),
	UNIQUE(fk_ADRESAS),
	CONSTRAINT pasiekiamas FOREIGN KEY(fk_ADRESAS) REFERENCES ADRESAI (id)
);

CREATE TABLE BILIETAI
(
	id int NOT NULL AUTO_INCREMENT,
	data date NOT NULL,
	kaina double NOT NULL,
	iejimas varchar (255) NOT NULL,
	blokas varchar (255) NOT NULL,
	eile varchar (255) NOT NULL,
	vieta int NOT NULL,
	fk_SIRGALIUS int NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT perka FOREIGN KEY(fk_SIRGALIUS) REFERENCES SIRGALIAI (id)
);

CREATE TABLE VARZYBOS
(
	id int NOT NULL AUTO_INCREMENT,
	data date NOT NULL,
	sirgaliu_kiekis int NOT NULL,
	rezultatas varchar (255) NOT NULL,
	fk_KOMANDA int NOT NULL,
	fk_TEISEJAS int NOT NULL,
	fk_KOMANDA1 int NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT zaidzia FOREIGN KEY(fk_KOMANDA) REFERENCES KOMANDOS (id),
	CONSTRAINT zaidzia2 FOREIGN KEY(fk_KOMANDA1) REFERENCES KOMANDOS (id),
	CONSTRAINT teisejauja FOREIGN KEY(fk_TEISEJAS) REFERENCES TEISEJAI (id)
);

CREATE TABLE ZAIDEJAI
(
	id int NOT NULL AUTO_INCREMENT,
	vardas varchar (255) NOT NULL,
	pavarde varchar (255) NOT NULL,
	gimimo_data date NOT NULL,
	tautybe varchar (255) NOT NULL,
	ugis double NOT NULL,
	svoris double NOT NULL,
	atlyginimas double NOT NULL,
	kontrakto_pabaiga date NOT NULL,
	rinkos_verte double NOT NULL,
	pozicija integer NOT NULL,
	stiprioji_koja integer NOT NULL,
	fk_KOMANDA int NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(pozicija) REFERENCES pozicija (id_pozicija),
	FOREIGN KEY(stiprioji_koja) REFERENCES stiprioji_koja (id_stiprioji_koja),
	CONSTRAINT atstovauja FOREIGN KEY(fk_KOMANDA) REFERENCES KOMANDOS (id)
);

CREATE TABLE IVARCIAI
(
	id int NOT NULL AUTO_INCREMENT,
	minute int,
	fk_ZAIDEJAS int NOT NULL,
	fk_VARZYBOS int NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT pelno FOREIGN KEY(fk_ZAIDEJAS) REFERENCES ZAIDEJAI (id),
	CONSTRAINT imusamas FOREIGN KEY(fk_VARZYBOS) REFERENCES VARZYBOS (id)
);

CREATE TABLE KORTELES
(
	id int NOT NULL AUTO_INCREMENT,
	geltona boolean NOT NULL,
	raudona boolean NOT NULL,
	minute int NOT NULL,
	fk_TEISEJAS int NOT NULL,
	fk_ZAIDEJAS int NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT skiria FOREIGN KEY(fk_TEISEJAS) REFERENCES TEISEJAI (id),
	CONSTRAINT baudziamas FOREIGN KEY(fk_ZAIDEJAS) REFERENCES ZAIDEJAI (id)
);

CREATE TABLE SIRGALIAI_VARZYBOS
(
	fk_SIRGALIUS int NOT NULL,
	fk_VARZYBOS int NOT NULL,
	PRIMARY KEY(fk_SIRGALIUS, fk_VARZYBOS),
	CONSTRAINT pritraukia FOREIGN KEY(fk_SIRGALIUS) REFERENCES SIRGALIAI (id),
	CONSTRAINT renkasi FOREIGN KEY(fk_VARZYBOS) REFERENCES VARZYBOS (id)
);

CREATE TABLE VARZYBOS_STADIONAI
(
	fk_STADIONAS int NOT NULL,
	fk_VARZYBOS int NOT NULL,
	PRIMARY KEY(fk_STADIONAS, fk_VARZYBOS),
	CONSTRAINT vyksta FOREIGN KEY(fk_STADIONAS) REFERENCES STADIONAI (id),
	CONSTRAINT vyksta2 FOREIGN KEY(fk_VARZYBOS) REFERENCES VARZYBOS (id)
);
