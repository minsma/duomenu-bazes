<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>&action=list">Varžybos</a></li>
	<li><?php if(!empty($id)) echo "Varžybų duomenų redagavimas"; else echo "Pridėti varžybas"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Varžybų informacija</legend>
            <input type="hidden" id="fk_KOMANDA" name="fk_KOMANDA" class="textbox textbox-150 date" value="<?php echo isset($data['fk_KOMANDA']) ? $data['fk_KOMANDA'] : ''; ?>">
            <input type="hidden" id="fk_TEISEJAS" name="fk_TEISEJAS" class="textbox textbox-150 date" value="<?php echo isset($data['fk_TEISEJAS']) ? $data['fk_TEISEJAS'] : ''; ?>">
            <input type="hidden" id="fk_KOMANDA1" name="fk_KOMANDA1" class="textbox textbox-150 date" value="<?php echo isset($data['fk_KOMANDA1']) ? $data['fk_KOMANDA1'] : ''; ?>">

            <p>
				<label class="field" for="data">Data<?php echo in_array('data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="data" name="data" class="textbox textbox-150 date" value="<?php echo isset($data['data']) ? $data['data'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="sirgaliu_kiekis">Sirgalių Kiekis<?php echo in_array('sirgaliu_kiekis', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="sirgaliu_kiekis" name="sirgaliu_kiekis" class="textbox textbox-150" value="<?php echo isset($data['sirgaliu_kiekis']) ? $data['sirgaliu_kiekis'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="rezultatas">Rezultatas<?php echo in_array('rezultatas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="rezultatas" name="rezultatas" class="textbox textbox-150" value="<?php echo isset($data['rezultatas']) ? $data['rezultatas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="teisejas">Teisėjas<?php echo in_array('teisejas', $required) ? '<span> *</span>' : ''; ?></label>
                <select id="fan" name="fk_TEISEJAS">
                    <option value="-1">Pasirinkite teiseją</option>
                    <?php
                    $refs = $referees->getList();
                    foreach($refs as $key => $val) {
                        $selected = "";
                        if(isset($data['fk_TEISEJAS']) && $data['fk_TEISEJAS'] == $val['id']) {
                            $selected = " selected='selected'";
                        }
                        echo "<option{$selected} value='{$val['id']}'>{$val['vardas']} {$val['pavarde']}</option>";
                    }
                    ?>
                </select>
            </p>
		</fieldset>
        <fieldset>
            <legend>Namų komandos informacija</legend>
            <p>
                <label class="field" for="pavadinimas">Pavadinimas<?php echo in_array('pavadinimas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="pavadinimas" name="pavadinimas" class="textbox textbox-150" value="<?php echo isset($data['pavadinimas']) ? $data['pavadinimas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="salis">Šalis<?php echo in_array('salis', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="salis" name="salis" class="textbox textbox-150" value="<?php echo isset($data['salis']) ? $data['salis'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="miestas">Miestas<?php echo in_array('miestas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="miestas" name="miestas" class="textbox textbox-150" value="<?php echo isset($data['miestas']) ? $data['miestas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="ikurimo_data">Įkūrimo data<?php echo in_array('ikurimo_data', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="ikurimo_data" name="ikurimo_data" class="textbox textbox-150 date" value="<?php echo isset($data['ikurimo_data']) ? $data['ikurimo_data'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="svetaine">Svetainė<?php echo in_array('svetaine', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="svetaine" name="svetaine" class="textbox textbox-150" value="<?php echo isset($data['svetaine']) ? $data['svetaine'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="biudzetas">Biudžetas<?php echo in_array('biudzetas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="number" id="biudzetas" name="biudzetas" class="textbox textbox-150" value="<?php echo isset($data['biudzetas']) ? $data['biudzetas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="treneris">Treneris<?php echo in_array('treneris', $required) ? '<span> *</span>' : ''; ?></label>
                <select id="team" name="fk_TRENERIS">
                    <option value="-1">Pasirinkite trenerį</option>
                    <?php
                    //$coach = $coaches->getListCoachesWithoutTeam();
                    $coach = $coaches->getList();
                    foreach($coach as $key => $val) {
                        $selected = "";
                        if(isset($data['fk_TRENERIS']) && $data['fk_TRENERIS'] == $val['id']) {
                            $selected = " selected='selected'";
                        }
                        echo "<option{$selected} value='{$val['id']}'>{$val['vardas']} {$val['pavarde']}</option>";
                    }
                    ?>
                </select>
            </p>
        </fieldset>
        <fieldset>
            <legend>Svečių komandos informacija</legend>
            <p>
                <label class="field" for="pavadinimas1">Pavadinimas<?php echo in_array('pavadinimas1', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="pavadinimas1" name="pavadinimas1" class="textbox textbox-150" value="<?php echo isset($data['pavadinimas1']) ? $data['pavadinimas1'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="salis1">Šalis<?php echo in_array('salis1', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="salis1" name="salis1" class="textbox textbox-150" value="<?php echo isset($data['salis1']) ? $data['salis1'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="miestas1">Miestas<?php echo in_array('miestas1', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="miestas1" name="miestas1" class="textbox textbox-150" value="<?php echo isset($data['miestas1']) ? $data['miestas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="ikurimo_data1">Įkūrimo data<?php echo in_array('ikurimo_data1', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="ikurimo_data1" name="ikurimo_data1" class="textbox textbox-150 date" value="<?php echo isset($data['ikurimo_data1']) ? $data['ikurimo_data1'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="svetaine1">Svetainė<?php echo in_array('svetaine1', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="svetaine1" name="svetaine1" class="textbox textbox-150" value="<?php echo isset($data['svetaine1']) ? $data['svetaine1'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="biudzetas1">Biudžetas<?php echo in_array('biudzetas1', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="number" id="biudzetas1" name="biudzetas1" class="textbox textbox-150" value="<?php echo isset($data['biudzetas1']) ? $data['biudzetas1'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="treneris1">Treneris<?php echo in_array('treneris1', $required) ? '<span> *</span>' : ''; ?></label>
                <select id="team" name="fk_TRENERIS1">
                    <option value="-1">Pasirinkite trenerį</option>
                    <?php
                    //$coach = $coaches->getListCoachesWithoutTeam();
                    $coach= $coaches->getList();
                    foreach($coach as $key => $val) {
                        $selected = "";
                        if(isset($data['fk_TRENERIS']) && $data['fk_TRENERIS'] == $val['id']) {
                            $selected = " selected='selected'";
                        }
                        echo "<option{$selected} value='{$val['id']}'>{$val['vardas']} {$val['pavarde']}</option>";
                    }
                    ?>
                </select>
            </p>
        </fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit button" name="submit" value="Išsaugoti">
		</p>
		<?php if(isset($data['id'])) { ?>
			<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
		<?php } ?>
	</form>
</div>