<ul id="pagePath">
    <li><a href="index.php">Pradžia</a></li>
    <li>Varžybos</li>
</ul>
<div id="actions">
    <a href='index.php?module=<?php echo $module; ?>&action=create'>Pridėti varžybas</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
    <div class="errorBox">
        Varžybos nebuvo pašalintas.
    </div>
<?php } ?>

<table class="listTable">
    <tr>
        <th>ID</th>
        <th>Data</th>
        <th>Namų Komanda</th>
        <th>Rezultatas</th>
        <th>Svečių komanda</th>
        <th>Teisėjas</th>
        <th></th>
    </tr>
    <?php
    foreach($data as $key => $val) {
        echo
            "<tr>"
            . "<td>{$val['id']}</td>"
            . "<td>{$val['data']}</td>"
            . "<td>{$val['1komanda']}</td>"
            . "<td>{$val['rezultatas']}</td>"
            . "<td>{$val['2komanda']}</td>"
            . "<td>{$val['teisejas']}</td>"
            . "<td>"
            . "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' title=''>šalinti</a>&nbsp;"
            . "<a href='index.php?module={$module}&action=edit&id={$val['id']}' title=''>redaguoti</a>"
            . "</td>"
            . "</tr>";
    }
    ?>
</table>

<?php
include 'templates/paging.tpl.php';
?>