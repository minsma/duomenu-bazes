<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>&action=list">Treneriai</a></li>
	<li><?php if(!empty($id)) echo "Sirgaliaus duomenų redagavimas"; else echo "Pridėti sirgalių"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Sirgaliaus informacija</legend>
            <p>
				<label class="field" for="vardas">Vardas<?php echo in_array('vardas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="vardas" name="vardas" class="textbox textbox-150" value="<?php echo isset($data['vardas']) ? $data['vardas'] : ''; ?>">
				<?php if(key_exists('vardas', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['vardas']} simb.)</span>"; ?>
            </p>
            <p>
                <label class="field" for="pavarde">Pavardė<?php echo in_array('pavarde', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="pavarde" name="pavarde" class="textbox textbox-150" value="<?php echo isset($data['pavarde']) ? $data['pavarde'] : ''; ?>">
                <?php if(key_exists('pavarde', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['pavarde']} simb.)</span>"; ?>
            </p>
            <p>
                <label class="field" for="telefono_numeris">Telefono numeris<?php echo in_array('telefono_numeris', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="telefono_numeris" name="telefono_numeris" class="textbox textbox-150" value="<?php echo isset($data['telefono_numeris']) ? $data['telefono_numeris'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="elektroninis_pastas">Elektroninis paštas<?php echo in_array('elektroninis_pastas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="elektroninis_pastas" name="elektroninis_pastas" class="textbox textbox-150" value="<?php echo isset($data['elektroninis_pastas']) ? $data['elektroninis_pastas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="gimimo_data">Gimimo data<?php echo in_array('gimimo_data', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="gimimo_data" name="gimimo_data" class="textbox textbox-150 date" value="<?php echo isset($data['gimimo_data']) ? $data['gimimo_data'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="adresas">Adresas<?php echo in_array('adresas', $required) ? '<span> *</span>' : ''; ?></label>
                <select id="fan" name="fk_ADRESAS">
                    <option value="-1">Pasirinkite adresą</option>
                    <?php
                    $addrs = $address->getList();
                    foreach($addrs as $key => $val) {
                        $selected = "";
                        if(isset($data['fk_ADRESAS']) && $data['fk_ADRESAS'] == $val['id']) {
                            $selected = " selected='selected'";
                        }
                        echo "<option{$selected} value='{$val['id']}'>{$val['salis']} {$val['miestas']} {$val['gatve']} 
                            {$val['pastato_nr']} {$val['zip_kodas']}</option>";
                    }
                    ?>
                </select>
            </p>
		</fieldset>
        <fieldset>
            <legend>Bilietai</legend>
            <div class="childRowContainer">
                <div class="labelLeft<?php if(empty($data['bilietai']) || sizeof($data['bilietai']) == 0) echo ' hidden'; ?>">Data</div>
                <div class="labelLeft<?php if(empty($data['bilietai']) || sizeof($data['bilietai']) == 0) echo ' hidden'; ?>">Kaina</div>
                <div class="labelLeft<?php if(empty($data['bilietai']) || sizeof($data['bilietai']) == 0) echo ' hidden'; ?>">Įėjimas</div>
                <div class="labelLeft<?php if(empty($data['bilietai']) || sizeof($data['bilietai']) == 0) echo ' hidden'; ?>">Blokas</div>
                <div class="labelLeft<?php if(empty($data['bilietai']) || sizeof($data['bilietai']) == 0) echo ' hidden'; ?>">Eilė</div>
                <div class="labelLeft<?php if(empty($data['bilietai']) || sizeof($data['bilietai']) == 0) echo ' hidden'; ?>">Vieta</div>
                <div class="labelLeft<?php if(empty($data['bilietai']) || sizeof($data['bilietai']) == 0) echo ' hidden'; ?>">Varžybos</div>
                <div class="float-clear"></div>
                <?php
                if(empty($data['bilietai']) || sizeof($data['bilietai']) == 0) {
                    ?>

                    <div class="childRow hidden">
                        <input type="text" name="datos[]" value="" class="textbox textbox-70" disabled="disabled" />
                        <input type="text" name="kainos[]" value="" class="textbox textbox-70" disabled="disabled" />
                        <input type="text" name="iejimai[]" value="" class="textbox textbox-70" disabled="disabled" />
                        <input type="text" name="blokai[]" value="" class="textbox textbox-70" disabled="disabled" />
                        <input type="text" name="eiles[]" value="" class="textbox textbox-70" disabled="disabled" />
                        <input type="text" name="vietos[]" value="" class="textbox textbox-70" disabled="disabled" />
                        <select id="fan" name="fk_VARZYBOS[]">
                            <option value="-1">Pasirinkite varžybas</option>
                            <?php
                            $match = $matches->getList();
                            foreach($match as $key => $val) {
                                $selected = "";
                                if(isset($data['fk_VARZYBOS']) && $data['fk_VARZYBOS'] == $val['id']) {
                                    $selected = " selected='selected'";
                                }
                                echo "<option{$selected} value='{$val['id']}'>{$val['data']}</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" class="isDisabledForEditing" name="neaktyvus[]" value="0" />
                        <a href="#" title="" class="removeChild">šalinti</a>
                    </div>
                    <div class="float-clear"></div>

                    <?php
                } else {
                    foreach($data['bilietai'] as $key => $val) {
                        ?>
                        <div class="childRow">
                            <input type="text" name="datos[]" value="<?php echo $val['data']; ?>" class="textbox textbox-70<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo ' disabledInput'; ?>" />
                            <input type="text" name="kainos[]" value="<?php echo $val['kaina']; ?>" class="textbox textbox-70<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo ' disabledInput'; ?>" />
                            <input type="text" name="iejimai[]" value="<?php echo $val['iejimas']; ?>" class="textbox textbox-70<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo ' disabledInput'; ?>" />
                            <input type="text" name="blokai[]" value="<?php echo $val['blokas']; ?>" class="textbox textbox-70<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo ' disabledInput'; ?>" />
                            <input type="text" name="eiles[]" value="<?php echo $val['eile']; ?>" class="textbox textbox-70<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo ' disabledInput'; ?>" />
                            <input type="text" name="vietos[]" value="<?php echo $val['vieta']; ?>" class="textbox textbox-70<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo ' disabledInput'; ?>" />
                            <select id="fan" name="fk_VARZYBOS[]">
                                <option value="-1">Pasirinkite varžybas</option>
                                <?php
                                $match = $matches->getList();
                                foreach($match as $key => $val) {
                                    $selected = "";
                                    if(isset($data['fk_VARZYBOS']) && $data['fk_VARZYBOS'] == $val['id']) {
                                        $selected = " selected='selected'";
                                    }
                                    echo "<option{$selected} value='{$val['id']}'>{$val['data']}</option>";
                                }
                                ?>
                            </select>

                            <input type="hidden" class="isDisabledForEditing" name="neaktyvus[]" value="<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo "1"; else echo "0"; ?>" />
                            <a href="#" title="" class="removeChild<?php if(isset($val['neaktyvus']) && $val['neaktyvus'] == 1) echo " hidden"; ?>">šalinti</a>
                        </div>
                        <div class="float-clear"></div>
                        <?php
                    }
                }
                ?>
            </div>
            <p id="newItemButtonContainer">
                <a href="#" title="" class="addChild">Pridėti</a>
            </p>
        </fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit button" name="submit" value="Išsaugoti">
		</p>
		<?php if(isset($data['id'])) { ?>
			<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
		<?php } ?>
	</form>
</div>