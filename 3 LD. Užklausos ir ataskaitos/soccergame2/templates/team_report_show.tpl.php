<ul id="reportInfo">
	<li class="title">Komandos žaidėjų turimų žaidėjų nurodytu laikotarpiu ataskaita</li>
	<li>Sudarymo data: <span><?php echo date("Y-m-d"); ?></span></li>
	<li>Komandos žaidėjų turimų kontraktų laikotarpis:
		<span>
		<?php
			if(!empty($data['dataNuo'])) {
				if(!empty($data['dataIki'])) {
					echo "nuo {$data['dataNuo']} iki {$data['dataIki']}";
				} else {
					echo "nuo {$data['dataNuo']}";
				}
			} else {
				if(!empty($data['dataIki'])) {
					echo "iki {$data['dataIki']}";
				} else {
					echo "nenurodyta";
				}
			}
		?>
		</span>
	</li>
</ul>
<?php
    if($teamsData != null) {
	if(sizeof($teamsData) > 0) { ?>
		<table class="reportTable">
			<tr class="gray">
				<th>ID</th>
				<th>Komanda</th>
				<th class="width100">Žaidėjų kiekis</th>
				<th class="width100">Turi sutartis už</th>
			</tr>

			<?php
				foreach($teamsData as $key => $val) {
					echo
						"<tr>"
							. "<td>{$val['id']}</td>"
							. "<td>{$val['pavadinimas']}</td>"
							. "<td>{$val['zaideju_kiekis']}</td>"
							. "<td>{$val['atlyginimu_suma']} &euro;</td>"
						. "</tr>";
				}
			?>

		  	<tr>
				<td class='groupSeparator' colspan='4'>Suma</td>
			</tr>

			<tr class="aggregate">
				<td></td>
				<td class="label"></td>
				<td class="border"><?php echo "{$teamsStats[0]['zaideju_kiekis']}"; ?></td>
				<td class="border"><?php echo "{$teamsStats[0]['atlyginimu_suma']}"; ?> &euro;</td>
			</tr>
		</table>
		<a href="index.php?module=team&action=report" title="Nauja ataskaita" style="margin-bottom: 15px" class="button large float-right">nauja ataskaita</a>
<?php
	}} else {
        ?>
        <div class="warningBox">
            Nurodytu laikotarpiu komandos žaidėjų neturi.
        </div>
        <?php
    }
?>