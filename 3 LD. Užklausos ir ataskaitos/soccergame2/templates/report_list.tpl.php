<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Ataskaitos</li>
</ul>
<div id="actions">
	
</div>
<div class="float-clear"></div>

<div class="page">
	<ul class="reportList">
		<li>
			<p>
				<a href="index.php?module=team&action=report" title="Žaidėjų ataskaita">Komandų ataskaita</a>
			</p>
			<p>Nurodytu laikotarpiu komandos turimų žaidėjų ataskaita.</p>
		</li>
        <li>
            <p>
                <a href="index.php?module=fan&action=report" title="Sirgalių ataskaita">Sirgalių ataskaita</a>
            </p>
            <p>Nurodytu laikotarpiu sirgaliaus turimų bilietų ataskaita.</p>
        </li>
	</ul>
</div>