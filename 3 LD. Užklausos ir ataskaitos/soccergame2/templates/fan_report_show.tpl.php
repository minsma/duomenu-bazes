<ul id="reportInfo">
    <li class="title">Sirgalių įsigytų bilietų ataskaita</li>
    <li>Sudarymo data: <span><?php echo date("Y-m-d"); ?></span></li>
    <li>Bilietų įsigijimo laikotarpis:
        <span>
		<?php
        if(!empty($data['dataNuo'])) {
            if(!empty($data['dataIki'])) {
                echo "nuo {$data['dataNuo']} iki {$data['dataIki']}";
            } else {
                echo "nuo {$data['dataNuo']}";
            }
        } else {
            if(!empty($data['dataIki'])) {
                echo "iki {$data['dataIki']}";
            } else {
                echo "nenurodyta";
            }
        }
        ?>
		</span>
    </li>
</ul>

<?php
if(sizeof($fansData) > 0) { ?>
    <table class="reportTable">
        <tr>
            <th>Varžybos</th>
            <th>Data</th>
            <th class="width150">Pirktas kiekis</th>
            <th class="width150">Išleista suma</th>
        </tr>

        <?php
        for($i = 0; $i < sizeof($fansData); $i++) {
            if($i == 0 || $fansData[$i]['sirgaliausId'] != $fansData[$i-1]['sirgaliausId']) {
                echo
                    "<tr>"
                    . "<td class='groupSeparator' colspan='4'>{$fansData[$i]['vardas']} {$fansData[$i]['pavarde']}</td>"
                    . "</tr>";
            }

            echo
                "<tr>"
                . "<td>#{$fansData[$i]['rungtynes_tarp']}</td>"
                . "<td>{$fansData[$i]['data']}</td>"
                . "<td>{$fansData[$i]['kiekis']} </td>"
                . "<td>{$fansData[$i]['kaina']}</td>"
                . "</tr>";

            if($i == (sizeof($fansData) - 1) || $fansData[$i]['sirgaliausId'] != $fansData[$i+1]['sirgaliausId']) {
                $fansData[$i]['bendra_uz_bilietus_uzdirbta_suma'] .= " &euro;";

                echo
                    "<tr class='aggregate'>"
                    . "<td colspan='2'></td>"
                    . "<td class='border'>Iš viso užsakyta kartų: {$fansData[$i]['bendras_parduotu_bilietu_kiekis']} </td>"
                    . "<td class='border'>{$fansData[$i]['bendra_uz_bilietus_uzdirbta_suma']}</td>"
                    . "</tr>";
            }
        }
        ?>

        <tr>
            <td class='groupSeparator' colspan='4'>Bendra suma</td>
        </tr>

        <tr class="aggregate">
            <td class="label" style="text-align: right" colspan="2"></td>
            <td class="border"><?php echo $ticketsCount[0]['bendras_kiekis']; ?> </td>
            <td class="border">
                <?php
                $ticketsPrice[0]['bendra_suma'] .= " &euro;";

                echo $ticketsPrice[0]['bendra_suma'];
                ?>
            </td>
        </tr>
    </table>
    <a href="index.php?module=fan&action=report" title="Nauja ataskaita" style="margin-bottom: 15px" class="button large float-right">Nauja ataskaita</a>
    <?php
} else {
    ?>
    <div class="warningBox">
        Nurodytu laikotarpiu bilietų parduota nebuvo.
    </div>
    <?php
}
?>