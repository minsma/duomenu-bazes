-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2018 at 11:18 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `futbl_varzybos`
--

-- --------------------------------------------------------

--
-- Table structure for table `adresai`
--

CREATE TABLE `adresai` (
  `id` int(11) NOT NULL,
  `salis` varchar(255) NOT NULL,
  `miestas` varchar(255) NOT NULL,
  `gatve` varchar(255) NOT NULL,
  `pastato_nr` int(11) NOT NULL,
  `zip_kodas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adresai`
--

INSERT INTO `adresai` (`id`, `salis`, `miestas`, `gatve`, `pastato_nr`, `zip_kodas`) VALUES
(1, 'Lietuva', 'Kaunas', 'Juozapaviciaus', 25, '255CD'),
(2, 'Lietuva', 'Vilnius', 'Kauno', 25, '14DED'),
(3, 'Lenkija', 'Varsuva', 'Poliski', 25, 'DE475');

-- --------------------------------------------------------

--
-- Table structure for table `bilietai`
--

CREATE TABLE `bilietai` (
  `id` int(11) NOT NULL,
  `data` date NOT NULL,
  `kaina` double NOT NULL,
  `iejimas` varchar(255) NOT NULL,
  `blokas` varchar(255) NOT NULL,
  `eile` varchar(255) NOT NULL,
  `vieta` int(11) NOT NULL,
  `fk_SIRGALIUS` int(11) DEFAULT NULL,
  `fk_VARZYBOS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bilietai`
--

INSERT INTO `bilietai` (`id`, `data`, `kaina`, `iejimas`, `blokas`, `eile`, `vieta`, `fk_SIRGALIUS`, `fk_VARZYBOS`) VALUES
(9, '2017-08-09', 25, '55', '42', '420', 36, 2, 1),
(10, '1996-10-11', 25, '55', '42', '420', 36, 2, 1),
(11, '1996-10-11', 25, '55', '42', '420', 36, 2, 1),
(12, '1996-10-11', 25, '55', '42', 'sadasdas', 36, 3, 1),
(13, '2017-08-09', 69, '55', '42', '420', 100, 3, 1),
(14, '1996-10-11', 25, 'adsaas', 'abc', 'sadasdas', 36, 3, 1),
(15, '1996-10-11', 25, '55', '42', '420', 36, 1, 1),
(16, '2017-08-09', 25, '55', '42', '420', 620, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ivarciai`
--

CREATE TABLE `ivarciai` (
  `id` int(11) NOT NULL,
  `minute` int(11) NOT NULL,
  `fk_ZAIDEJAS` int(11) NOT NULL,
  `fk_VARZYBOS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komandos`
--

CREATE TABLE `komandos` (
  `id` int(11) NOT NULL,
  `pavadinimas` varchar(255) NOT NULL,
  `salis` varchar(255) NOT NULL,
  `miestas` varchar(255) NOT NULL,
  `ikurimo_data` date NOT NULL,
  `svetaine` varchar(255) NOT NULL,
  `biudzetas` double NOT NULL,
  `fk_TRENERIS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komandos`
--

INSERT INTO `komandos` (`id`, `pavadinimas`, `salis`, `miestas`, `ikurimo_data`, `svetaine`, `biudzetas`, `fk_TRENERIS`) VALUES
(1, 'Real', 'Ispanija', 'Madridas', '1940-05-05', 'https://www.realmadrid.com/en', 962000000, 1),
(2, 'Barcelona', 'Ispanija', 'Barcelona', '1945-05-05', 'https://www.fcbarcelona.com/', 800000000, 2),
(3, 'Bayern', 'Vokietija', 'Miunchenas', '1940-05-05', 'https://fcbayern.com/en', 800000000, 3),
(4, 'Chelsea', 'Anglija', 'Londonas', '1930-05-05', 'https://www.chelseafc.com', 800000000, 4);

-- --------------------------------------------------------

--
-- Table structure for table `korteles`
--

CREATE TABLE `korteles` (
  `id` int(11) NOT NULL,
  `geltona` tinyint(1) NOT NULL,
  `raudona` tinyint(1) NOT NULL,
  `minute` int(11) NOT NULL,
  `fk_ZAIDEJAS` int(11) NOT NULL,
  `fk_TEISEJAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pozicija`
--

CREATE TABLE `pozicija` (
  `id_pozicija` int(11) NOT NULL,
  `name` char(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pozicija`
--

INSERT INTO `pozicija` (`id_pozicija`, `name`) VALUES
(1, 'puolejas'),
(2, 'saugas'),
(3, 'gynejas'),
(4, 'vartininkas');

-- --------------------------------------------------------

--
-- Table structure for table `sirgaliai`
--

CREATE TABLE `sirgaliai` (
  `id` int(11) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `telefono_numeris` varchar(255) NOT NULL,
  `elektroninis_pastas` varchar(255) NOT NULL,
  `gimimo_data` date NOT NULL,
  `fk_ADRESAS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sirgaliai`
--

INSERT INTO `sirgaliai` (`id`, `vardas`, `pavarde`, `telefono_numeris`, `elektroninis_pastas`, `gimimo_data`, `fk_ADRESAS`) VALUES
(1, 'Petras', 'Petraitis', '86666666666', 'petras@petraitis.lt', '1954-05-05', 1),
(2, 'Jonas', 'Jonaitis', '8666665475', 'jonas@jonaitis.lt', '1956-05-06', NULL),
(3, 'klkjk', 'jhkjjh', '545446546', 'sadas@gmail.com', '2016-01-12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stadionai`
--

CREATE TABLE `stadionai` (
  `id` int(11) NOT NULL,
  `pavadinimas` varchar(255) NOT NULL,
  `telefono_numeris` varchar(255) NOT NULL,
  `elektroninis_pastas` varchar(255) NOT NULL,
  `talpa` int(11) NOT NULL,
  `fk_ADRESAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stadionai_varzybos`
--

CREATE TABLE `stadionai_varzybos` (
  `fk_STADIONAS` int(11) NOT NULL,
  `fk_VARZYBOS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stiprioji_koja`
--

CREATE TABLE `stiprioji_koja` (
  `id_stiprioji_koja` int(11) NOT NULL,
  `name` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stiprioji_koja`
--

INSERT INTO `stiprioji_koja` (`id_stiprioji_koja`, `name`) VALUES
(1, 'kaire'),
(2, 'desine'),
(3, 'abi');

-- --------------------------------------------------------

--
-- Table structure for table `teisejai`
--

CREATE TABLE `teisejai` (
  `id` int(11) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `gimimo_data` date NOT NULL,
  `tautybe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teisejai`
--

INSERT INTO `teisejai` (`id`, `vardas`, `pavarde`, `gimimo_data`, `tautybe`) VALUES
(1, 'Howard', 'Stern', '1950-12-05', 'Anglas'),
(2, 'John', 'Dee', '1968-10-05', 'Amerikietis');

-- --------------------------------------------------------

--
-- Table structure for table `treneriai`
--

CREATE TABLE `treneriai` (
  `id` int(11) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `gimimo_data` date NOT NULL,
  `tautybe` varchar(255) NOT NULL,
  `ugis` double NOT NULL,
  `atlyginimas` double NOT NULL,
  `kontrakto_pabaiga` date NOT NULL,
  `rinkos_verte` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treneriai`
--

INSERT INTO `treneriai` (`id`, `vardas`, `pavarde`, `gimimo_data`, `tautybe`, `ugis`, `atlyginimas`, `kontrakto_pabaiga`, `rinkos_verte`) VALUES
(1, 'Zinedine', 'Zidane', '1972-05-05', 'Prancuzas', 185, 12500000, '2018-10-05', 12500000),
(2, 'Ernesto', 'Valverde', '1964-05-06', 'Ispanas', 175, 80000000, '0000-00-00', 80000000),
(3, 'Jupp', 'Heynckes', '1945-05-05', 'Vokietis', 178, 10000000, '2019-06-30', 10000000),
(4, 'Antonio', 'Conte', '2016-07-16', 'Italas', 185, 10000000, '0000-00-00', 10000000);

-- --------------------------------------------------------

--
-- Table structure for table `varzybos`
--

CREATE TABLE `varzybos` (
  `id` int(11) NOT NULL,
  `data` date NOT NULL,
  `sirgaliu_kiekis` int(11) NOT NULL,
  `rezultatas` varchar(255) NOT NULL,
  `fk_TEISEJAS` int(11) NOT NULL,
  `fk_KOMANDA` int(11) NOT NULL,
  `fk_KOMANDA1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `varzybos`
--

INSERT INTO `varzybos` (`id`, `data`, `sirgaliu_kiekis`, `rezultatas`, `fk_TEISEJAS`, `fk_KOMANDA`, `fk_KOMANDA1`) VALUES
(1, '2017-08-08', 40000, '3:2', 1, 1, 2),
(2, '2018-06-25', 60000, '3:0', 1, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `zaidejai`
--

CREATE TABLE `zaidejai` (
  `id` int(11) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `gimimo_data` date NOT NULL,
  `tautybe` varchar(255) NOT NULL,
  `ugis` double NOT NULL,
  `svoris` double NOT NULL,
  `atlyginimas` double NOT NULL,
  `kontrakto_pabaiga` date NOT NULL,
  `rinkos_verte` double NOT NULL,
  `pozicija` int(11) NOT NULL,
  `stiprioji_koja` int(11) NOT NULL,
  `fk_KOMANDA` int(11) DEFAULT NULL,
  `kontrakto_pradzia` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zaidejai`
--

INSERT INTO `zaidejai` (`id`, `vardas`, `pavarde`, `gimimo_data`, `tautybe`, `ugis`, `svoris`, `atlyginimas`, `kontrakto_pabaiga`, `rinkos_verte`, `pozicija`, `stiprioji_koja`, `fk_KOMANDA`, `kontrakto_pradzia`) VALUES
(1, 'Cristiano', 'Ronaldo', '1985-02-05', 'Portugalas', 185, 85, 120000000, '2021-06-30', 150000000, 1, 3, 1, '2017-06-30'),
(2, 'Pasibaiges', 'Kontraktas', '1985-02-05', 'Portugalas', 185, 85, 120000000, '2016-06-30', 150000000, 1, 3, 1, '2015-06-30'),
(3, 'Pirmas', 'Pirmas', '1995-10-10', 'Lietuvis', 185, 85, 1000, '2020-06-30', 1000, 1, 1, 2, '2017-06-30'),
(4, 'Antras', 'Antras', '1995-10-10', 'Lietuvis', 185, 85, 1000, '2018-06-30', 1000, 1, 1, 2, '2017-06-30'),
(5, 'Trecias', 'Trecias', '1995-10-10', 'Lietuvis', 185, 85, 1000, '2014-06-30', 1000, 1, 1, 2, '2013-06-30'),
(6, 'Ketvirtas', 'Ketvirtas', '1995-10-10', 'Lietuvis', 185, 85, 1000, '2014-06-30', 1000, 1, 1, 2, '2013-06-30'),
(7, 'Penktas', 'Penktas', '1995-10-10', 'Lietuvis', 185, 85, 1000, '2017-06-30', 1000, 1, 1, 3, '2019-06-30'),
(8, 'Sestas', 'Sestas', '1995-10-10', 'Lietuvis', 185, 85, 1000, '2017-06-30', 1000, 1, 1, 3, '2019-06-30'),
(9, 'Septintas', 'Septintas', '1995-10-10', 'Lietuvis', 185, 85, 1000, '2015-06-30', 1000, 1, 1, 4, '2017-06-30'),
(10, 'Astuntas', 'Astuntas', '1995-10-10', 'Lietuvis', 185, 85, 1000, '2018-06-30', 1000, 1, 1, 4, '2022-06-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adresai`
--
ALTER TABLE `adresai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bilietai`
--
ALTER TABLE `bilietai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perka` (`fk_SIRGALIUS`),
  ADD KEY `prekiaujama` (`fk_VARZYBOS`);

--
-- Indexes for table `ivarciai`
--
ALTER TABLE `ivarciai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pelno` (`fk_ZAIDEJAS`),
  ADD KEY `imusamas` (`fk_VARZYBOS`);

--
-- Indexes for table `komandos`
--
ALTER TABLE `komandos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fk_TRENERIS` (`fk_TRENERIS`);

--
-- Indexes for table `korteles`
--
ALTER TABLE `korteles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baudziamas` (`fk_ZAIDEJAS`),
  ADD KEY `skiria` (`fk_TEISEJAS`);

--
-- Indexes for table `pozicija`
--
ALTER TABLE `pozicija`
  ADD PRIMARY KEY (`id_pozicija`);

--
-- Indexes for table `sirgaliai`
--
ALTER TABLE `sirgaliai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gyvena` (`fk_ADRESAS`);

--
-- Indexes for table `stadionai`
--
ALTER TABLE `stadionai`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fk_ADRESAS` (`fk_ADRESAS`);

--
-- Indexes for table `stadionai_varzybos`
--
ALTER TABLE `stadionai_varzybos`
  ADD PRIMARY KEY (`fk_STADIONAS`,`fk_VARZYBOS`),
  ADD KEY `vyksta2` (`fk_VARZYBOS`);

--
-- Indexes for table `stiprioji_koja`
--
ALTER TABLE `stiprioji_koja`
  ADD PRIMARY KEY (`id_stiprioji_koja`);

--
-- Indexes for table `teisejai`
--
ALTER TABLE `teisejai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treneriai`
--
ALTER TABLE `treneriai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `varzybos`
--
ALTER TABLE `varzybos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teisejauja` (`fk_TEISEJAS`),
  ADD KEY `zaidzia` (`fk_KOMANDA`),
  ADD KEY `zaidzia2` (`fk_KOMANDA1`);

--
-- Indexes for table `zaidejai`
--
ALTER TABLE `zaidejai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pozicija` (`pozicija`),
  ADD KEY `stiprioji_koja` (`stiprioji_koja`),
  ADD KEY `atstovauja` (`fk_KOMANDA`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adresai`
--
ALTER TABLE `adresai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bilietai`
--
ALTER TABLE `bilietai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ivarciai`
--
ALTER TABLE `ivarciai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `komandos`
--
ALTER TABLE `komandos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `korteles`
--
ALTER TABLE `korteles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sirgaliai`
--
ALTER TABLE `sirgaliai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stadionai`
--
ALTER TABLE `stadionai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teisejai`
--
ALTER TABLE `teisejai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `treneriai`
--
ALTER TABLE `treneriai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `varzybos`
--
ALTER TABLE `varzybos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `zaidejai`
--
ALTER TABLE `zaidejai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bilietai`
--
ALTER TABLE `bilietai`
  ADD CONSTRAINT `perka` FOREIGN KEY (`fk_SIRGALIUS`) REFERENCES `sirgaliai` (`id`),
  ADD CONSTRAINT `prekiaujama` FOREIGN KEY (`fk_VARZYBOS`) REFERENCES `varzybos` (`id`);

--
-- Constraints for table `ivarciai`
--
ALTER TABLE `ivarciai`
  ADD CONSTRAINT `imusamas` FOREIGN KEY (`fk_VARZYBOS`) REFERENCES `varzybos` (`id`),
  ADD CONSTRAINT `pelno` FOREIGN KEY (`fk_ZAIDEJAS`) REFERENCES `zaidejai` (`id`);

--
-- Constraints for table `komandos`
--
ALTER TABLE `komandos`
  ADD CONSTRAINT `treniruoja` FOREIGN KEY (`fk_TRENERIS`) REFERENCES `treneriai` (`id`);

--
-- Constraints for table `korteles`
--
ALTER TABLE `korteles`
  ADD CONSTRAINT `baudziamas` FOREIGN KEY (`fk_ZAIDEJAS`) REFERENCES `zaidejai` (`id`),
  ADD CONSTRAINT `skiria` FOREIGN KEY (`fk_TEISEJAS`) REFERENCES `teisejai` (`id`);

--
-- Constraints for table `sirgaliai`
--
ALTER TABLE `sirgaliai`
  ADD CONSTRAINT `gyvena` FOREIGN KEY (`fk_ADRESAS`) REFERENCES `adresai` (`id`);

--
-- Constraints for table `stadionai`
--
ALTER TABLE `stadionai`
  ADD CONSTRAINT `pasiekiamas` FOREIGN KEY (`fk_ADRESAS`) REFERENCES `adresai` (`id`);

--
-- Constraints for table `stadionai_varzybos`
--
ALTER TABLE `stadionai_varzybos`
  ADD CONSTRAINT `vyksta` FOREIGN KEY (`fk_STADIONAS`) REFERENCES `stadionai` (`id`),
  ADD CONSTRAINT `vyksta2` FOREIGN KEY (`fk_VARZYBOS`) REFERENCES `varzybos` (`id`);

--
-- Constraints for table `varzybos`
--
ALTER TABLE `varzybos`
  ADD CONSTRAINT `teisejauja` FOREIGN KEY (`fk_TEISEJAS`) REFERENCES `teisejai` (`id`),
  ADD CONSTRAINT `zaidzia` FOREIGN KEY (`fk_KOMANDA`) REFERENCES `komandos` (`id`),
  ADD CONSTRAINT `zaidzia2` FOREIGN KEY (`fk_KOMANDA1`) REFERENCES `komandos` (`id`);

--
-- Constraints for table `zaidejai`
--
ALTER TABLE `zaidejai`
  ADD CONSTRAINT `atstovauja` FOREIGN KEY (`fk_KOMANDA`) REFERENCES `komandos` (`id`),
  ADD CONSTRAINT `zaidejai_ibfk_1` FOREIGN KEY (`pozicija`) REFERENCES `pozicija` (`id_pozicija`),
  ADD CONSTRAINT `zaidejai_ibfk_2` FOREIGN KEY (`stiprioji_koja`) REFERENCES `stiprioji_koja` (`id_stiprioji_koja`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
