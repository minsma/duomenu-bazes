<?php

class coaches {
	
	private $coach_table = '';

	public function __construct() {
		$this->coach_table = 'treneriai';
		$this->teams_table = 'komandos';
	}

	public function get($id) {
		$query = "  SELECT *
					FROM {$this->coach_table}
					WHERE `id`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}

	public function getList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
			
			if(isset($offset)) {
				$limitOffsetString .= " OFFSET {$offset}";
			}	
		}
		
		$query = "  SELECT *
					FROM {$this->coach_table}{$limitOffsetString}";
		$data = mysql::select($query);
		
		return $data;
	}

    public function getListCoachesWithoutTeam($limit = null, $offset = null){
        $limitOffsetString = "";
        if(isset($limit)) {
            $limitOffsetString .= " LIMIT {$limit}";

            if(isset($offset)) {
                $limitOffsetString .= " OFFSET {$offset}";
            }
        }

        $query = "  SELECT *
					FROM {$this->coach_table}
					WHERE NOT EXISTS (
					  SELECT * 
					  FROM {$this->teams_table}
					  WHERE {$this->teams_table}.`fk_TRENERIS` = {$this->coach_table}.`id`
					){$limitOffsetString}";
        $data = mysql::select($query);

        return $data;
    }

	public function getListCount() {
		$query = "  SELECT COUNT(`id`) as `kiekis`
					FROM {$this->coach_table}";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}

	public function insert($data) {
		$query = "  INSERT INTO {$this->coach_table}
								(
									`vardas`,
									`pavarde`,
									`gimimo_data`,
									`tautybe`,
									`ugis`,
									`atlyginimas`,
									`kontrakto_pabaiga`,
									`rinkos_verte`
								)
								VALUES
								(
									'{$data['vardas']}',
									'{$data['pavarde']}',
									'{$data['gimimo_data']}',
									'{$data['tautybe']}',
									'{$data['ugis']}',
									'{$data['atlyginimas']}',
									'{$data['kontrakto_pabaiga']}',
									'{$data['rinkos_verte']}'
								)";
		mysql::query($query);
	}

	public function update($data) {
		$query = "  UPDATE {$this->coach_table}
					SET    `vardas`='{$data['vardas']}',
					       `pavarde`='{$data['pavarde']}',
					       `gimimo_data`='{$data['gimimo_data']}',
					       `tautybe`='{$data['tautybe']}',
					       `ugis`='{$data['ugis']}',
					       `atlyginimas`='{$data['atlyginimas']}',
					       `kontrakto_pabaiga`='{$data['kontrakto_pabaiga']}',
					       `rinkos_verte`='{$data['rinkos_verte']}'
					WHERE `id`='{$data['id']}'";
		mysql::query($query);
	}

	public function delete($id) {
		$query = "  DELETE FROM {$this->coach_table}
					WHERE `id`='{$id}'";
		mysql::query($query);
	}

    public function doesCoachHaveTeam($id) {
        $query = "  SELECT *
					FROM {$this->teams_table}
					WHERE {$this->teams_table}.`fk_TRENERIS`='{$id}'";
        $data = mysql::select($query);

        if($data != null)
            return true;

        return false;
	}
}