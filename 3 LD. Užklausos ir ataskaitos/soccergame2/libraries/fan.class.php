<?php

class fans
{
    private $fan_table = '';
    private $ticket_table = '';
    private $fan_match_table = '';
    private $match_table = '';

    public function __construct()
    {
        $this->fan_table = 'sirgaliai';
        $this->ticket_table = 'bilietai';
        $this->fan_match_table = 'sirgaliai_varzybos';
        $this->match_table = 'varzybos';
    }

    public function get($id)
    {
        $query = "  SELECT *
					FROM {$this->fan_table}
					WHERE `id`='{$id}'";
        $data = mysql::select($query);

        return $data[0];
    }

    public function getList($limit = null, $offset = null)
    {
        $limitOffsetString = "";
        if (isset($limit)) {
            $limitOffsetString .= " LIMIT {$limit}";

            if (isset($offset)) {
                $limitOffsetString .= " OFFSET {$offset}";
            }
        }

        $query = "SELECT  sirgaliai.id,
                          sirgaliai.vardas,
                          sirgaliai.pavarde,
                          sirgaliai.telefono_numeris,
                          sirgaliai.elektroninis_pastas,
                          sirgaliai.gimimo_data,
                          sirgaliai.fk_ADRESAS,
                          COUNT(bilietai.id) as kiekis
                  FROM sirgaliai
                  LEFT JOIN bilietai ON sirgaliai.id = bilietai.fk_SIRGALIUS
                  GROUP BY sirgaliai.id
                  {$limitOffsetString}
                  ";

        $data = mysql::select($query);

        return $data;
    }

    public function getListCount()
    {
        $query = "  SELECT COUNT(`id`) as `kiekis`
					FROM {$this->fan_table}";
        $data = mysql::select($query);

        return $data[0]['kiekis'];
    }

    public function insert($data)
    {
        if($data['fk_ADRESAS'] >= 0) {
            $query = "  INSERT INTO {$this->fan_table}
                                        (
                                            `vardas`,
                                            `pavarde`,
                                            `telefono_numeris`,
                                            `elektroninis_pastas`,
                                            `gimimo_data`,
                                            `fk_ADRESAS`
                                        )
                                        VALUES
                                        (
                                            '{$data['vardas']}',
                                            '{$data['pavarde']}',
                                            '{$data['telefono_numeris']}',
                                            '{$data['elektroninis_pastas']}',
                                            '{$data['gimimo_data']}',
                                            '{$data['fk_ADRESAS']}'
                                        )";
            mysql::query($query);
        } else {
            $query = "  INSERT INTO {$this->fan_table}
                                        (
                                            `vardas`,
                                            `pavarde`,
                                            `telefono_numeris`,
                                            `elektroninis_pastas`,
                                            `gimimo_data`
                                        )
                                        VALUES
                                        (
                                            '{$data['vardas']}',
                                            '{$data['pavarde']}',
                                            '{$data['telefono_numeris']}',
                                            '{$data['elektroninis_pastas']}',
                                            '{$data['gimimo_data']}'
                                        )";
            mysql::query($query);
        }

        return mysql::getLastInsertedId();
    }

    public function getTickets($Id) {
        $query = "  SELECT *
					FROM `{$this->ticket_table}`
					WHERE `fk_SIRGALIUS`='{$Id}'";
        $data = mysql::select($query);

        return $data;
    }

    public function insertTickets($data) {
        if(isset($data['datos']) && sizeof($data['datos']) > 0) {
            foreach($data['datos'] as $key=>$val) {
                    $query = "  INSERT INTO `{$this->ticket_table}`
											(
											    `data`,
											    `kaina`,
											    `iejimas`,
											    `blokas`,
											    `eile`,
											    `vieta`,
											    `fk_SIRGALIUS`,
											    `fk_VARZYBOS`
											)
											VALUES
											(
											    '{$val}',
											    '{$data['kainos'][$key]}',
											    '{$data['iejimai'][$key]}',
											    '{$data['blokai'][$key]}',
											    '{$data['eiles'][$key]}',
											    '{$data['vietos'][$key]}',
												'{$data['id']}',
												'{$data['fk_VARZYBOS'][$key]}'
											)";
                    mysql::query($query);
            }
        }
    }

    public function deleteTickets($Id) {
        $query = "  DELETE FROM `{$this->ticket_table}`
					WHERE `fk_SIRGALIUS`='{$Id}'";
        mysql::query($query);
    }

    public function update($data)
    {
        $query = "  UPDATE {$this->fan_table}
					SET    `vardas`='{$data['vardas']}',
					       `pavarde`='{$data['pavarde']}',
					       `telefono_numeris`='{$data['telefono_numeris']}',
					       `elektroninis_pastas`='{$data['elektroninis_pastas']}',
					       `gimimo_data`='{$data['gimimo_data']}',
					       `fk_ADRESAS`='{$data['fk_ADRESAS']}'
					WHERE `id`='{$data['id']}'";
        mysql::query($query);
    }

    public function delete($id)
    {
        $query = "  DELETE FROM {$this->fan_table}
					WHERE `id`='{$id}'";
        mysql::query($query);
    }

    public function isUsedInOtherTableAsForeignKey($id) {
        $query = "  SELECT *
					FROM {$this->fan_match_table}
					WHERE {$this->fan_match_table}.`fk_SIRGALIUS`='{$id}'";
        $data = mysql::select($query);

        if($data != null)
            return true;

        return false;
    }

    public function getFansData($dateFrom, $dateTo) {
        $whereClauseString = "";
        if(!empty($dateFrom)) {
            $whereClauseString .= " WHERE `{$this->match_table}`.`data`>='{$dateFrom}'";
            if(!empty($dateTo)) {
                $whereClauseString .= " AND `{$this->match_table}`.`data`<='{$dateTo}'";
            }
        } else {
            if(!empty($dateTo)) {
                $whereClauseString .= " WHERE `{$this->match_table}`.`data`<='{$dateTo}'";
            }
        }

        $query = " SELECT   sirgaliai.id as sirgaliausId,
                            sirgaliai.vardas, 
                            sirgaliai.pavarde, 
                            sum(bilietai.kaina) as kaina, 
                            varzybos.data, 
                            count(varzybos.id) as kiekis,
                            ad.adresas,
                            k.rungtynes_tarp,
                            t.bendra_uz_bilietus_uzdirbta_suma,
                            s.bendras_parduotu_bilietu_kiekis
                    FROM bilietai
                        INNER JOIN varzybos ON bilietai.fk_VARZYBOS = varzybos.id
                        INNER JOIN (
                            sirgaliai LEFT JOIN adresai ON sirgaliai.fk_ADRESAS = adresai.id  
                        ) ON bilietai.fk_SIRGALIUS = sirgaliai.id
                        LEFT JOIN (
                            SELECT  varzybos.id as v,
                                    CONCAT(komandos1.pavadinimas, ' - ', komandos2.pavadinimas) as rungtynes_tarp
                            FROM varzybos
                            INNER JOIN komandos komandos1 ON varzybos.fk_KOMANDA = komandos1.id
                            INNER JOIN komandos komandos2 ON varzybos.fk_KOMANDA1 = komandos2.id
                            {$whereClauseString}
                        ) k on k.v = varzybos.id
                        LEFT JOIN (
                            SELECT adresai.id as adr,
                                   CONCAT(adresai.salis, ' ', adresai.miestas, ' ', adresai.gatve, ' ', adresai.pastato_nr) as adresas
                            FROM sirgaliai
                            INNER JOIN adresai ON sirgaliai.id = adresai.id
                        ) ad ON ad.adr = adresai.id
                        LEFT JOIN (
                            SELECT sirgaliai.id AS sirgalius,
                                    IFNULL(sum(bilietai.kaina), 0) as bendra_uz_bilietus_uzdirbta_suma
                            FROM bilietai
                            INNER JOIN varzybos
                                ON bilietai.fk_VARZYBOS = varzybos.id
                            INNER JOIN (
                                sirgaliai LEFT JOIN adresai ON sirgaliai.fk_ADRESAS = adresai.id  
                            ) ON bilietai.fk_SIRGALIUS = sirgaliai.id
                            {$whereClauseString}						
                            GROUP BY sirgaliai.id
                        ) t ON t.sirgalius = sirgaliai.id
                        LEFT JOIN (
                            SELECT sirgaliai.id AS sirgalius,
                                    count(bilietai.id) as bendras_parduotu_bilietu_kiekis
                            FROM bilietai
                            INNER JOIN varzybos
                                ON bilietai.fk_VARZYBOS = varzybos.id
                            INNER JOIN (
                                sirgaliai LEFT JOIN adresai ON sirgaliai.fk_ADRESAS = adresai.id  
                            ) ON bilietai.fk_SIRGALIUS = sirgaliai.id						
                            GROUP BY sirgaliai.id
                        ) s ON s.sirgalius = sirgaliai.id
                        {$whereClauseString}
                        GROUP BY sirgaliai.id, varzybos.id";
        $data = mysql::select($query);

        return $data;
    }

    public function getSumPriceOfTicketsForAllFans($dateFrom, $dateTo) {
        $whereClauseString = "";
        if(!empty($dateFrom)) {
            $whereClauseString .= " WHERE `{$this->match_table}`.`data`>='{$dateFrom}'";
            if(!empty($dateTo)) {
                $whereClauseString .= " AND `{$this->match_table}`.`data`<='{$dateTo}'";
            }
        } else {
            if(!empty($dateTo)) {
                $whereClauseString .= " WHERE `{$this->match_table}`.`data`<='{$dateTo}'";
            }
        }

        $query = "  SELECT sum(bilietai.kaina) as bendra_suma
                   FROM bilietai
                   INNER JOIN sirgaliai ON sirgaliai.id = bilietai.fk_SIRGALIUS 
                   INNER JOIN varzybos ON varzybos.id = bilietai.fk_VARZYBOS
					{$whereClauseString}";
        $data = mysql::select($query);

        return $data;
    }

    public function getCountOfTicketsForAllFans($dateFrom, $dateTo) {
        $whereClauseString = "";
        if(!empty($dateFrom)) {
            $whereClauseString .= " WHERE `{$this->match_table}`.`data`>='{$dateFrom}'";
            if(!empty($dateTo)) {
                $whereClauseString .= " AND `{$this->match_table}`.`data`<='{$dateTo}'";
            }
        } else {
            if(!empty($dateTo)) {
                $whereClauseString .= " WHERE `{$this->match_table}`.`data`<='{$dateTo}'";
            }
        }

        $query = "  SELECT count(bilietai.id) as bendras_kiekis
                       FROM bilietai
                       INNER JOIN sirgaliai ON sirgaliai.id = bilietai.fk_SIRGALIUS 
                       INNER JOIN varzybos ON varzybos.id = bilietai.fk_VARZYBOS
					{$whereClauseString}";
        $data = mysql::select($query);

        return $data;
    }
}