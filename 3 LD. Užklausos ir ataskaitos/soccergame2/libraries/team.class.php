<?php

class teams {

    private $team_table = '';
    private $coach_table = '';
    private $match_table = '';
    private $player_table = '';

    public function __construct() {
        $this->team_table = 'komandos';
        $this->coach_table = 'treneriai';
        $this->match_table = 'varzybos';
        $this->player_table = 'zaidejai';
    }

    public function get($id) {
        $query = "  SELECT *
					FROM `{$this->team_table}`
					WHERE `id`='{$id}'";
        $data = mysql::select($query);

        return $data[0];
    }

    public function getList($limit = null, $offset = null) {
        $limitOffsetString = "";
        if(isset($limit)) {
            $limitOffsetString .= " LIMIT {$limit}";

            if(isset($offset)) {
                $limitOffsetString .= " OFFSET {$offset}";
            }
        }

        $query = "  SELECT `{$this->team_table}`.`id`,
						   `{$this->team_table}`.`pavadinimas`,
						   `{$this->team_table}`.`salis`,
						   `{$this->team_table}`.`miestas`,
						   `{$this->team_table}`.`ikurimo_data`,
						   `{$this->team_table}`.`svetaine`,
						   `{$this->team_table}`.`biudzetas`,
						    CONCAT(`{$this->coach_table}`.`vardas`, ' ', `{$this->coach_table}`.`pavarde`) AS `treneris`
					FROM `{$this->team_table}`
						LEFT JOIN `{$this->coach_table}`
							ON `{$this->team_table}`.`fk_TRENERIS`=`{$this->coach_table}`.`id`{$limitOffsetString}";
        $data = mysql::select($query);

        return $data;
    }

    public function getListCount() {
        $query = "  SELECT COUNT(`{$this->team_table}`.`id`) as `kiekis`
					FROM `{$this->team_table}`
						LEFT JOIN `{$this->coach_table}`
							ON `{$this->team_table}`.`fk_TRENERIS`=`{$this->coach_table}`.`id`";
        $data = mysql::select($query);

        return $data[0]['kiekis'];
    }

    public function update($data) {
        if($data['fk_TRENERIS'] > 0) {
            $query = "  UPDATE `{$this->team_table}`
					SET    `pavadinimas`='{$data['pavadinimas']}',
					       `salis`='{$data['salis']}',
					       `miestas`='{$data['miestas']}',
					       `ikurimo_data`='{$data['ikurimo_data']}',
					       `svetaine`='{$data['svetaine']}',
					       `biudzetas`='{$data['biudzetas']}',
						   `fk_TRENERIS`='{$data['fk_TRENERIS']}'
					WHERE `id`='{$data['id']}'";
            mysql::query($query);
        }
        else {
            $query = "  UPDATE `{$this->team_table}`
					SET    `pavadinimas`='{$data['pavadinimas']}',
					       `salis`='{$data['salis']}',
					       `miestas`='{$data['miestas']}',
					       `ikurimo_data`='{$data['ikurimo_data']}',
					       `svetaine`='{$data['svetaine']}',
					       `biudzetas`='{$data['biudzetas']}'
					WHERE `id`='{$data['id']}'";
            mysql::query($query);
        }
    }

    public function insert($data) {
        if($data['fk_TRENERIS'] >= 0){
            $query = "  INSERT INTO `{$this->team_table}`
                                    (
                                        `pavadinimas`,
                                        `salis`,
                                        `miestas`,
                                        `ikurimo_data`,
                                        `svetaine`,
                                        `biudzetas`,
                                        `fk_TRENERIS`
                                    )
                                    VALUES
                                    (
                                        '{$data['pavadinimas']}',
                                        '{$data['salis']}',
                                        '{$data['miestas']}',
                                        '{$data['ikurimo_data']}',
                                        '{$data['svetaine']}',
                                        '{$data['biudzetas']}',
                                        '{$data['fk_TRENERIS']}'
                                    )";
            mysql::query($query);
        } else {
            $query = "  INSERT INTO `{$this->team_table}`
                                    (
                                        `pavadinimas`,
                                        `salis`,
                                        `miestas`,
                                        `ikurimo_data`,
                                        `svetaine`,
                                        `biudzetas`
                                    )
                                    VALUES
                                    (
                                        '{$data['pavadinimas']}',
                                        '{$data['salis']}',
                                        '{$data['miestas']}',
                                        '{$data['ikurimo_data']}',
                                        '{$data['svetaine']}',
                                        '{$data['biudzetas']}'
                                    )";
            mysql::query($query);
        }

        return mysql::getLastInsertedId();
    }

    public function delete($id) {
        $query = "  DELETE FROM `{$this->team_table}`
					WHERE `id`='{$id}'";
        mysql::query($query);
    }

    public function doesTeamHaveMatches($id) {
        $query = "  SELECT *
					FROM {$this->match_table}
					WHERE {$this->match_table}.`fk_KOMANDA`='{$id}'
					OR {$this->match_table}.`fk_KOMANDA1`='{$id}'";
        $data = mysql::select($query);

        if($data != null)
            return true;

        return false;
    }

    public function getOrderedTeams($dateFrom, $dateTo) {
        $whereClauseString = "";

        if(!empty($dateFrom)) {
            if(!empty($dateTo)) {
                $whereClauseString = "WHERE zaidejai.kontrakto_pradzia >= '{$dateFrom}' AND zaidejai.kontrakto_pabaiga <= '{$dateTo}'
                    OR zaidejai.kontrakto_pradzia >= '{$dateFrom}' AND zaidejai.kontrakto_pabaiga >= '{$dateFrom}'
                    OR zaidejai.kontrakto_pradzia <= '{$dateFrom}' AND zaidejai.kontrakto_pabaiga <= '{$dateTo}' AND zaidejai.kontrakto_pabaiga >= '{$dateFrom}'
                    OR zaidejai.kontrakto_pradzia <= '{$dateFrom}' AND zaidejai.kontrakto_pabaiga >= '{$dateTo}' AND zaidejai.kontrakto_pradzia <= '{$dateTo}'";
            }
        } else {
            if(!empty($dateTo)) {
                $whereClauseString .= " WHERE `{$this->player_table}`.`kontrakto_pabaiga`<='{$dateTo}'";
            }
        }

        $query = "  SELECT komandos.id, pavadinimas, count(*) as zaideju_kiekis, sum(atlyginimas) as atlyginimu_suma
                    from komandos
                    INNER JOIN zaidejai
                    ON zaidejai.fk_KOMANDA = komandos.id
                    {$whereClauseString}
                    GROUP BY komandos.id
                    ORDER BY zaideju_kiekis ASC, pavadinimas DESC";

        $data = mysql::select($query);

        return $data;
    }

    public function getStatsOfOrderedTeams($dateFrom, $dateTo) {
        $whereClauseString = "";

        if(!empty($dateFrom)) {
            if(!empty($dateTo)) {
                $whereClauseString = "WHERE zaidejai.kontrakto_pradzia >= '{$dateFrom}' AND zaidejai.kontrakto_pabaiga <= '{$dateTo}'
                    OR zaidejai.kontrakto_pradzia >= '{$dateFrom}' AND zaidejai.kontrakto_pabaiga >= '{$dateFrom}'
                    OR zaidejai.kontrakto_pradzia <= '{$dateFrom}' AND zaidejai.kontrakto_pabaiga <= '{$dateTo}' AND zaidejai.kontrakto_pabaiga >= '{$dateFrom}'
                    OR zaidejai.kontrakto_pradzia <= '{$dateFrom}' AND zaidejai.kontrakto_pabaiga >= '{$dateTo}' AND zaidejai.kontrakto_pradzia <= '{$dateTo}'";
            }
        } else {
            if(!empty($dateTo)) {
                $whereClauseString .= " WHERE `{$this->player_table}`.`kontrakto_pabaiga`<='{$dateTo}'";
            }
        }

        $query = "  SELECT count(*) as zaideju_kiekis, sum(atlyginimas) as atlyginimu_suma
                    from zaidejai
                    {$whereClauseString}                    
                    ORDER BY atlyginimu_suma DESC";

        $data = mysql::select($query);

        return $data;
    }
}