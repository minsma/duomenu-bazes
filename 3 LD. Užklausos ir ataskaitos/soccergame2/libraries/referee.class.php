<?php

class referees {

    private $referees_table = '';
    private $matches_table = '';

    public function __construct() {
        $this->referees_table = 'teisejai';
        $this->matches_table = 'varzybos';
    }

    public function get($id) {
        $query = "  SELECT *
					FROM {$this->referees_table}
					WHERE `id`='{$id}'";
        $data = mysql::select($query);

        return $data[0];
    }

    public function getList($limit = null, $offset = null) {
        $limitOffsetString = "";
        if(isset($limit)) {
            $limitOffsetString .= " LIMIT {$limit}";

            if(isset($offset)) {
                $limitOffsetString .= " OFFSET {$offset}";
            }
        }

        $query = "  SELECT *
					FROM {$this->referees_table}{$limitOffsetString}";
        $data = mysql::select($query);

        return $data;
    }

    public function getListCount() {
        $query = "  SELECT COUNT(`id`) as `kiekis`
					FROM {$this->referees_table}";
        $data = mysql::select($query);

        return $data[0]['kiekis'];
    }

    public function insert($data) {
        $query = "  INSERT INTO {$this->referees_table}
								(
									`vardas`,
									`pavarde`,
									`gimimo_data`,
									`tautybe`
								)
								VALUES
								(
									'{$data['vardas']}',
									'{$data['pavarde']}',
									'{$data['gimimo_data']}',
									'{$data['tautybe']}'
								)";
        mysql::query($query);
    }

    public function update($data) {
        $query = "  UPDATE {$this->referees_table}
					SET    `vardas`='{$data['vardas']}',
					       `pavarde`='{$data['pavarde']}',
					       `gimimo_data`='{$data['gimimo_data']}',
					       `tautybe`='{$data['tautybe']}'
					WHERE `id`='{$data['id']}'";
        mysql::query($query);
    }

    public function delete($id) {
        $query = "  DELETE FROM {$this->referees_table}
					WHERE `id`='{$id}'";
        mysql::query($query);
    }

    public function doesHaveMatches($id) {
        $query = "  SELECT *
					FROM {$this->matches_table}
					WHERE {$this->matches_table}.`fk_TEISEJAS`='{$id}'";
        $data = mysql::select($query);

        if($data != null)
            return true;

        return false;
    }
}