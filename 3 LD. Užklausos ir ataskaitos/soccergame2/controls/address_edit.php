<?php

include 'libraries/address.class.php';
$address = new address();

$formErrors = null;
$data = array();

$required = array(
    'salis',
    'miestas',
    'gatve',
    'pastato_nr',
    'zip_kodas'
);

if(!empty($_POST['submit'])) {
    $validations = array (
        'salis' => 'alfanum',
        'miestas' => 'alfanum',
        'gatve' => 'alfanum',
        'pastato_nr' => 'int',
        'zip_kodas' => 'anything'
    );


    include 'utils/validator.class.php';
	$validator = new validator($validations, $required);

	if($validator->validate($_POST)) {
		$dataPrepared = $validator->preparePostFieldsForSQL();

        $address->update($dataPrepared);

		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		$formErrors = $validator->getErrorHTML();
		$data = $_POST;
	}
} else {
	$data = $address->get($id);
}

include 'templates/address_form.tpl.php';

?>