<?php

include 'libraries/fan.class.php';
$fans = new fans();

include 'libraries/address.class.php';
$address = new address();

include 'libraries/match.class.php';
$matches = new matches();

$formErrors = null;
$data = array();

$required = array(
    'vardas',
    'pavarde',
    'telefono_numeris',
    'elektroninis_pastas',
    'gimimo_data',
    'datos',
    'kainos',
    'iejimai',
    'blokai',
    'eiles',
    'vietos',
    'fk_VARZYBOS'
);

$maxLengths = array (
	'vardas' => 20,
    'pavarde' => 20
);

if(!empty($_POST['submit'])) {
	$validations = array (
        'vardas' => 'alfanum',
        'pavarde' => 'alfanum',
        'telefono_numeris' => 'phone',
        'elektroninis_pastas' => 'email',
        'gimimo_data' => 'date',
        'datos' => 'date',
        'kainos' => 'price',
        'iejimai' => 'anything',
        'blokai' => 'anything',
        'eiles' => 'anything',
        'vietos' => 'int',
        'fk_VARZYBOS' => 'positivenumber'
    );

	include 'utils/validator.class.php';
	$validator = new validator($validations, $required, $maxLengths);

	if($validator->validate($_POST)) {
		$dataPrepared = $validator->preparePostFieldsForSQL();

        $dataPrepared['id'] = $fans->insert($dataPrepared);

        $fans->insertTickets($dataPrepared);

		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		$formErrors = $validator->getErrorHTML();
		$data = $_POST;
	}
}

include 'templates/fan_form.tpl.php';

?>