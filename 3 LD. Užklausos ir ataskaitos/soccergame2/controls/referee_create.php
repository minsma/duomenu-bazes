<?php

include 'libraries/referee.class.php';
$referees = new referees();

$formErrors = null;
$data = array();

$required = array(
    'vardas',
    'pavarde',
    'gimimo_data',
    'tautybe'
);

$maxLengths = array (
	'vardas' => 20,
    'pavarde' => 20
);

if(!empty($_POST['submit'])) {
	$validations = array (
        'vardas' => 'alfanum',
        'pavarde' => 'alfanum',
        'gimimo_data' => 'date',
        'tautybe' => 'alfanum'
    );

	include 'utils/validator.class.php';
	$validator = new validator($validations, $required, $maxLengths);

	if($validator->validate($_POST)) {
		$dataPrepared = $validator->preparePostFieldsForSQL();

        $referees->insert($dataPrepared);

		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		$formErrors = $validator->getErrorHTML();
		$data = $_POST;
	}
}

include 'templates/referee_form.tpl.php';

?>