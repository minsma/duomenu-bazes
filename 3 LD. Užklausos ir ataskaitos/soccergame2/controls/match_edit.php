<?php

include 'libraries/match.class.php';
$matches = new matches();

include 'libraries/referee.class.php';
$referees = new referees();

include 'libraries/team.class.php';
$teams = new teams();

include 'libraries/coach.class.php';
$coaches = new coaches();

$formErrors = null;
$data = array();

$required = array(
    'data', 'sirgaliu_kiekis', 'rezultatas',
    'fk_TEISEJAS', 'pavadinimas', 'salis',
    'miestas', 'ikurimo_data', 'svetaine',
    'biudzetas', 'pavadinimas1', 'salis1',
    'miestas1', 'ikurimo_data1', 'svetaine1',
    'biudzetas1'
);

if(!empty($_POST['submit'])) {
    $validations = array (
        'data'              => 'date',
        'sirgaliu_kiekis'   => 'int',
        'rezultatas'        => 'anything',
        'fk_TEISEJAS'       => 'positivenumber',
        'pavadinimas'       => 'alfanum',
        'salis'             => 'alfanum',
        'miestas'           => 'alfanum',
        'ikurimo_data'      => 'date',
        'svetaine'          => 'alfanum',
        'biudzetas'         => 'positivenumber',
        'pavadinimas1'       => 'alfanum',
        'salis1'             => 'alfanum',
        'miestas1'           => 'alfanum',
        'ikurimo_data1'      => 'date',
        'svetaine1'          => 'alfanum',
        'biudzetas1'         => 'positivenumber'
    );

	include 'utils/validator.class.php';
	$validator = new validator($validations, $required);

	if($validator->validate($_POST)) {
		$dataPrepared = $validator->preparePostFieldsForSQL();

        $dat = [
            'id'            => $dataPrepared['fk_KOMANDA'],
            'pavadinimas'   => $dataPrepared['pavadinimas'],
            'salis'         => $dataPrepared['salis'],
            'miestas'       => $dataPrepared['miestas'],
            'ikurimo_data'  => $dataPrepared['ikurimo_data'],
            'svetaine'      => $dataPrepared['svetaine'],
            'biudzetas'     => $dataPrepared['biudzetas'],
            'fk_TRENERIS'   => $dataPrepared['fk_TRENERIS']
        ];

		$teams->update($dat);

        $dat = [
            'id'            => $dataPrepared['fk_KOMANDA1'],
            'pavadinimas'   => $dataPrepared['pavadinimas1'],
            'salis'         => $dataPrepared['salis1'],
            'miestas'       => $dataPrepared['miestas1'],
            'ikurimo_data'  => $dataPrepared['ikurimo_data1'],
            'svetaine'      => $dataPrepared['svetaine1'],
            'biudzetas'     => $dataPrepared['biudzetas1'],
            'fk_TRENERIS'   => $dataPrepared['fk_TRENERIS1']
        ];

        $teams->update($dat);

        $matches->update($dataPrepared);

		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		$formErrors = $validator->getErrorHTML();
		$data = $_POST;
	}
} else {
    $data = $matches->get($id);

    $first_team = $teams->get($data['fk_KOMANDA']);

    $data = array_merge($data, $first_team);
    $second_team = $teams->get($data['fk_KOMANDA1']);
    $referee = $referees->get($data['fk_TEISEJAS']);

    $secnd_team = [
        'pavadinimas1'   => $second_team['pavadinimas'],
        'salis1'         => $second_team['salis'],
        'miestas1'       => $second_team['miestas'],
        'ikurimo_data1'  => $second_team['ikurimo_data'],
        'svetaine1'      => $second_team['svetaine'],
        'biudzetas1'     => $second_team['biudzetas'],
        'fk_TRENERIS1'   => $second_team['fk_TRENERIS']
    ];

    $data = array_merge($data, $first_team);
    $data = array_merge($data, $secnd_team);
    $data = array_merge($data, $referee);
}

include 'templates/match_form.tpl.php';

?>