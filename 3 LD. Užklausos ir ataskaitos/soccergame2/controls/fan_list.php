<?php

include 'libraries/fan.class.php';
$fans = new fans();

$elementCount = $fans->getListCount();

include 'utils/paging.class.php';
$paging = new paging(config::NUMBER_OF_ROWS_IN_PAGE);

$paging->process($elementCount, $pageId);

$data = $fans->getList($paging->size, $paging->first);

include 'templates/fan_list.tpl.php';

?>