<?php

include 'libraries/fan.class.php';
$fans = new fans();

$formErrors = null;
$fields = array();
$formSubmitted = false;

$data = array();
if(empty($_POST['submit'])) {
	include 'templates/fan_report_form.tpl.php';
} else {
	$formSubmitted = true;
	
	$validations = array (
			'dataNuo' => 'date',
			'dataIki' => 'date');
	
	include 'utils/validator.class.php';
	$validator = new validator($validations);
	
	if($validator->validate($_POST)) {
		$data = $validator->preparePostFieldsForSQL();

		$fansData = $fans->getFansData($data['dataNuo'], $data['dataIki']);
		$ticketsPrice = $fans->getSumPriceOfTicketsForAllFans($data['dataNuo'], $data['dataIki']);
		$ticketsCount = $fans->getCountOfTicketsForAllFans($data['dataNuo'], $data['dataIki']);

		include 'templates/fan_report_show.tpl.php';
	} else {
		$formErrors = $validator->getErrorHTML();
		$fields = $_POST;
		
		include 'templates/fan_report_form.tpl.php';
	}
}

