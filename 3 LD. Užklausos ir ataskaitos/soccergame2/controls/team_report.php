<?php

include 'libraries/team.class.php';
$teams = new teams();

$formErrors = null;
$fields = array();
$formSubmitted = false;

$data = array();
if(empty($_POST['submit'])) {
	include 'templates/team_report_form.tpl.php';
} else {
	$formSubmitted = true;
	
	$validations = array (
			'dataNuo' => 'date',
			'dataIki' => 'date');
	
	include 'utils/validator.class.php';
	$validator = new validator($validations);
	
	if($validator->validate($_POST)) {
		$data = $validator->preparePostFieldsForSQL();
		
		$teamsData = $teams->getOrderedTeams($data['dataNuo'], $data['dataIki']);
		$teamsStats = $teams->getStatsOfOrderedTeams($data['dataNuo'], $data['dataIki']);
		
		include 'templates/team_report_show.tpl.php';
	} else {
		$formErrors = $validator->getErrorHTML();
		$fields = $_POST;
		
		include 'templates/team_report_form.tpl.php';
	}
}

