<?php

include 'libraries/match.class.php';
$matches = new matches();

include 'libraries/referee.class.php';
$referees = new referees();

include 'libraries/team.class.php';
$teams = new teams();

include 'libraries/coach.class.php';
$coaches = new coaches();

$formErrors = null;
$data = array();

$required = array(
    'data', 'sirgaliu_kiekis', 'rezultatas',
    'fk_TEISEJAS', 'pavadinimas', 'salis',
    'miestas', 'ikurimo_data', 'svetaine',
    'biudzetas', 'pavadinimas1', 'salis1',
    'miestas1', 'ikurimo_data1', 'svetaine1',
    'biudzetas1'
);

if(!empty($_POST['submit'])) {
    $validations = array (
        'data'              => 'date',
        'sirgaliu_kiekis'   => 'int',
        'rezultatas'        => 'anything',
        'fk_TEISEJAS'       => 'positivenumber',
        'pavadinimas'       => 'alfanum',
        'salis'             => 'alfanum',
        'miestas'           => 'alfanum',
        'ikurimo_data'      => 'date',
        'svetaine'          => 'alfanum',
        'biudzetas'         => 'positivenumber',
        'pavadinimas1'       => 'alfanum',
        'salis1'             => 'alfanum',
        'miestas1'           => 'alfanum',
        'ikurimo_data1'      => 'date',
        'svetaine1'          => 'alfanum',
        'biudzetas1'         => 'positivenumber'
    );

	include 'utils/validator.class.php';
	$validator = new validator($validations, $required);

	if($validator->validate($_POST)) {
		$dataPrepared = $validator->preparePostFieldsForSQL();

        $dataPrepared['fk_KOMANDA'] = $teams->insert($dataPrepared);

        $dat = [
            'pavadinimas'   => $dataPrepared['pavadinimas1'],
            'salis'         => $dataPrepared['salis1'],
            'miestas'       => $dataPrepared['miestas1'],
            'ikurimo_data'  => $dataPrepared['ikurimo_data1'],
            'svetaine'      => $dataPrepared['svetaine1'],
            'biudzetas'     => $dataPrepared['biudzetas1'],
            'fk_TRENERIS'   => $dataPrepared['fk_TRENERIS1']
        ];

        $dataPrepared['fk_KOMANDA1'] = $teams->insert($dat);

        $matches->insert($dataPrepared);

		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		$formErrors = $validator->getErrorHTML();
		$data = $_POST;
	}
}

include 'templates/match_form.tpl.php';

?>