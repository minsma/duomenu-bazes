<?php

include 'libraries/coach.class.php';
$coaches = new coaches();

$elementCount = $coaches->getListCount();

include 'utils/paging.class.php';
$paging = new paging(config::NUMBER_OF_ROWS_IN_PAGE);

$paging->process($elementCount, $pageId);

$data = $coaches->getList($paging->size, $paging->first);

include 'templates/coach_list.tpl.php';

?>