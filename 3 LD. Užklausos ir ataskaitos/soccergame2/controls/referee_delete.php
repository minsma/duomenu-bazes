<?php

include 'libraries/referee.class.php';
$referees = new referees();

if(!empty($id)) {
	$doesHaveMatch= $referees->doesHaveMatches($id);

	$removeErrorParameter = '';
	if($doesHaveMatch == false) {
        $referees->delete($id);
	} else {
		$removeErrorParameter = '&remove_error=1';
	}

	header("Location: index.php?module={$module}&action=list{$removeErrorParameter}");
	die();
}

?>