-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2018 at 09:15 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `futbl_varzybos`
--

-- --------------------------------------------------------

--
-- Table structure for table `adresai`
--

CREATE TABLE `adresai` (
  `id` int(11) NOT NULL,
  `salis` varchar(255) NOT NULL,
  `miestas` varchar(255) NOT NULL,
  `gatve` varchar(255) NOT NULL,
  `pastato_nr` int(11) NOT NULL,
  `zip_kodas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bilietai`
--

CREATE TABLE `bilietai` (
  `id` int(11) NOT NULL,
  `data` date NOT NULL,
  `kaina` double NOT NULL,
  `iejimas` varchar(255) NOT NULL,
  `blokas` varchar(255) NOT NULL,
  `eile` varchar(255) NOT NULL,
  `vieta` int(11) NOT NULL,
  `fk_SIRGALIUS` int(11) DEFAULT NULL,
  `fk_VARZYBOS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ivarciai`
--

CREATE TABLE `ivarciai` (
  `id` int(11) NOT NULL,
  `minute` int(11) NOT NULL,
  `fk_ZAIDEJAS` int(11) NOT NULL,
  `fk_VARZYBOS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komandos`
--

CREATE TABLE `komandos` (
  `id` int(11) NOT NULL,
  `pavadinimas` varchar(255) NOT NULL,
  `salis` varchar(255) NOT NULL,
  `miestas` varchar(255) NOT NULL,
  `ikurimo_data` date NOT NULL,
  `svetaine` varchar(255) NOT NULL,
  `biudzetas` double NOT NULL,
  `fk_TRENERIS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `korteles`
--

CREATE TABLE `korteles` (
  `id` int(11) NOT NULL,
  `geltona` tinyint(1) NOT NULL,
  `raudona` tinyint(1) NOT NULL,
  `minute` int(11) NOT NULL,
  `fk_ZAIDEJAS` int(11) NOT NULL,
  `fk_TEISEJAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pozicija`
--

CREATE TABLE `pozicija` (
  `id_pozicija` int(11) NOT NULL,
  `name` char(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pozicija`
--

INSERT INTO `pozicija` (`id_pozicija`, `name`) VALUES
(1, 'puolejas'),
(2, 'saugas'),
(3, 'gynejas'),
(4, 'vartininkas');

-- --------------------------------------------------------

--
-- Table structure for table `sirgalius`
--

CREATE TABLE `sirgalius` (
  `id` int(11) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `telefono_numeris` varchar(255) NOT NULL,
  `elektroninis_pastas` varchar(255) NOT NULL,
  `gimimo_data` date NOT NULL,
  `fk_ADRESAS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stadionai`
--

CREATE TABLE `stadionai` (
  `id` int(11) NOT NULL,
  `pavadinimas` varchar(255) NOT NULL,
  `telefono_numeris` varchar(255) NOT NULL,
  `elektroninis_pastas` varchar(255) NOT NULL,
  `talpa` int(11) NOT NULL,
  `fk_ADRESAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stadionai_varzybos`
--

CREATE TABLE `stadionai_varzybos` (
  `fk_STADIONAS` int(11) NOT NULL,
  `fk_VARZYBOS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stiprioji_koja`
--

CREATE TABLE `stiprioji_koja` (
  `id_stiprioji_koja` int(11) NOT NULL,
  `name` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stiprioji_koja`
--

INSERT INTO `stiprioji_koja` (`id_stiprioji_koja`, `name`) VALUES
(1, 'kaire'),
(2, 'desine'),
(3, 'abi');

-- --------------------------------------------------------

--
-- Table structure for table `teisejai`
--

CREATE TABLE `teisejai` (
  `id` int(11) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `gimimo_data` date NOT NULL,
  `tautybe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teisejai`
--

INSERT INTO `teisejai` (`id`, `vardas`, `pavarde`, `gimimo_data`, `tautybe`) VALUES
(1, 'James', 'Gordon', '1950-05-05', 'Britas');

-- --------------------------------------------------------

--
-- Table structure for table `treneriai`
--

CREATE TABLE `treneriai` (
  `id` int(11) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `gimimo_data` date NOT NULL,
  `tautybe` varchar(255) NOT NULL,
  `ugis` double NOT NULL,
  `atlyginimas` double NOT NULL,
  `kontrakto_pabaiga` date NOT NULL,
  `rinkos_verte` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treneriai`
--

INSERT INTO `treneriai` (`id`, `vardas`, `pavarde`, `gimimo_data`, `tautybe`, `ugis`, `atlyginimas`, `kontrakto_pabaiga`, `rinkos_verte`) VALUES
(1, 'Brian', 'Clough', '1950-10-10', 'Britas', 185, 200000, '2019-10-10', 2000000),
(2, 'Johan', 'Cruyff', '1950-10-10', 'Amerikietis', 175, 200000, '2018-10-05', 20000000);

-- --------------------------------------------------------

--
-- Table structure for table `varzybos`
--

CREATE TABLE `varzybos` (
  `id` int(11) NOT NULL,
  `data` date NOT NULL,
  `sirgaliu_kiekis` int(11) NOT NULL,
  `rezultatas` varchar(255) NOT NULL,
  `fk_TEISEJAS` int(11) NOT NULL,
  `fk_KOMANDA` int(11) NOT NULL,
  `fk_KOMANDA1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zaidejai`
--

CREATE TABLE `zaidejai` (
  `id` int(11) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `gimimo_data` date NOT NULL,
  `tautybe` varchar(255) NOT NULL,
  `ugis` double NOT NULL,
  `svoris` double NOT NULL,
  `atlyginimas` double NOT NULL,
  `kontrakto_pabaiga` date NOT NULL,
  `rinkos_verte` double NOT NULL,
  `pozicija` int(11) NOT NULL,
  `stiprioji_koja` int(11) NOT NULL,
  `fk_KOMANDA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adresai`
--
ALTER TABLE `adresai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bilietai`
--
ALTER TABLE `bilietai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perka` (`fk_SIRGALIUS`),
  ADD KEY `prekiaujama` (`fk_VARZYBOS`);

--
-- Indexes for table `ivarciai`
--
ALTER TABLE `ivarciai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pelno` (`fk_ZAIDEJAS`),
  ADD KEY `imusamas` (`fk_VARZYBOS`);

--
-- Indexes for table `komandos`
--
ALTER TABLE `komandos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fk_TRENERIS` (`fk_TRENERIS`);

--
-- Indexes for table `korteles`
--
ALTER TABLE `korteles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baudziamas` (`fk_ZAIDEJAS`),
  ADD KEY `skiria` (`fk_TEISEJAS`);

--
-- Indexes for table `pozicija`
--
ALTER TABLE `pozicija`
  ADD PRIMARY KEY (`id_pozicija`);

--
-- Indexes for table `sirgalius`
--
ALTER TABLE `sirgalius`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gyvena` (`fk_ADRESAS`);

--
-- Indexes for table `stadionai`
--
ALTER TABLE `stadionai`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fk_ADRESAS` (`fk_ADRESAS`);

--
-- Indexes for table `stadionai_varzybos`
--
ALTER TABLE `stadionai_varzybos`
  ADD PRIMARY KEY (`fk_STADIONAS`,`fk_VARZYBOS`),
  ADD KEY `vyksta2` (`fk_VARZYBOS`);

--
-- Indexes for table `stiprioji_koja`
--
ALTER TABLE `stiprioji_koja`
  ADD PRIMARY KEY (`id_stiprioji_koja`);

--
-- Indexes for table `teisejai`
--
ALTER TABLE `teisejai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treneriai`
--
ALTER TABLE `treneriai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `varzybos`
--
ALTER TABLE `varzybos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teisejauja` (`fk_TEISEJAS`),
  ADD KEY `zaidzia` (`fk_KOMANDA`),
  ADD KEY `zaidzia2` (`fk_KOMANDA1`);

--
-- Indexes for table `zaidejai`
--
ALTER TABLE `zaidejai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pozicija` (`pozicija`),
  ADD KEY `stiprioji_koja` (`stiprioji_koja`),
  ADD KEY `atstovauja` (`fk_KOMANDA`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adresai`
--
ALTER TABLE `adresai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bilietai`
--
ALTER TABLE `bilietai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ivarciai`
--
ALTER TABLE `ivarciai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `komandos`
--
ALTER TABLE `komandos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `korteles`
--
ALTER TABLE `korteles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sirgalius`
--
ALTER TABLE `sirgalius`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stadionai`
--
ALTER TABLE `stadionai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teisejai`
--
ALTER TABLE `teisejai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `treneriai`
--
ALTER TABLE `treneriai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `varzybos`
--
ALTER TABLE `varzybos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `zaidejai`
--
ALTER TABLE `zaidejai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bilietai`
--
ALTER TABLE `bilietai`
  ADD CONSTRAINT `perka` FOREIGN KEY (`fk_SIRGALIUS`) REFERENCES `sirgalius` (`id`),
  ADD CONSTRAINT `prekiaujama` FOREIGN KEY (`fk_VARZYBOS`) REFERENCES `varzybos` (`id`);

--
-- Constraints for table `ivarciai`
--
ALTER TABLE `ivarciai`
  ADD CONSTRAINT `imusamas` FOREIGN KEY (`fk_VARZYBOS`) REFERENCES `varzybos` (`id`),
  ADD CONSTRAINT `pelno` FOREIGN KEY (`fk_ZAIDEJAS`) REFERENCES `zaidejai` (`id`);

--
-- Constraints for table `komandos`
--
ALTER TABLE `komandos`
  ADD CONSTRAINT `treniruoja` FOREIGN KEY (`fk_TRENERIS`) REFERENCES `treneriai` (`id`);

--
-- Constraints for table `korteles`
--
ALTER TABLE `korteles`
  ADD CONSTRAINT `baudziamas` FOREIGN KEY (`fk_ZAIDEJAS`) REFERENCES `zaidejai` (`id`),
  ADD CONSTRAINT `skiria` FOREIGN KEY (`fk_TEISEJAS`) REFERENCES `teisejai` (`id`);

--
-- Constraints for table `sirgalius`
--
ALTER TABLE `sirgalius`
  ADD CONSTRAINT `gyvena` FOREIGN KEY (`fk_ADRESAS`) REFERENCES `adresai` (`id`);

--
-- Constraints for table `stadionai`
--
ALTER TABLE `stadionai`
  ADD CONSTRAINT `pasiekiamas` FOREIGN KEY (`fk_ADRESAS`) REFERENCES `adresai` (`id`);

--
-- Constraints for table `stadionai_varzybos`
--
ALTER TABLE `stadionai_varzybos`
  ADD CONSTRAINT `vyksta` FOREIGN KEY (`fk_STADIONAS`) REFERENCES `stadionai` (`id`),
  ADD CONSTRAINT `vyksta2` FOREIGN KEY (`fk_VARZYBOS`) REFERENCES `varzybos` (`id`);

--
-- Constraints for table `varzybos`
--
ALTER TABLE `varzybos`
  ADD CONSTRAINT `teisejauja` FOREIGN KEY (`fk_TEISEJAS`) REFERENCES `teisejai` (`id`),
  ADD CONSTRAINT `zaidzia` FOREIGN KEY (`fk_KOMANDA`) REFERENCES `komandos` (`id`),
  ADD CONSTRAINT `zaidzia2` FOREIGN KEY (`fk_KOMANDA1`) REFERENCES `komandos` (`id`);

--
-- Constraints for table `zaidejai`
--
ALTER TABLE `zaidejai`
  ADD CONSTRAINT `atstovauja` FOREIGN KEY (`fk_KOMANDA`) REFERENCES `komandos` (`id`),
  ADD CONSTRAINT `zaidejai_ibfk_1` FOREIGN KEY (`pozicija`) REFERENCES `pozicija` (`id_pozicija`),
  ADD CONSTRAINT `zaidejai_ibfk_2` FOREIGN KEY (`stiprioji_koja`) REFERENCES `stiprioji_koja` (`id_stiprioji_koja`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
