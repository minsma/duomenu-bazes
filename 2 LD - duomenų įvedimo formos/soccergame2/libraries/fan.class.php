<?php

class fans
{
    private $fan_table = '';
    private $ticket_table = '';
    private $fan_match_table = '';

    public function __construct()
    {
        $this->fan_table = 'sirgaliai';
        $this->ticket_table = 'bilietai';
        $this->fan_match_table = 'sirgaliai_varzybos';
    }

    public function get($id)
    {
        $query = "  SELECT *
					FROM {$this->fan_table}
					WHERE `id`='{$id}'";
        $data = mysql::select($query);

        return $data[0];
    }

    public function getList($limit = null, $offset = null)
    {
        $limitOffsetString = "";
        if (isset($limit)) {
            $limitOffsetString .= " LIMIT {$limit}";

            if (isset($offset)) {
                $limitOffsetString .= " OFFSET {$offset}";
            }
        }

//        $query = "  SELECT *
//					FROM {$this->fan_table} {$limitOffsetString}";
        $query = "SELECT *, COUNT(bilietai.id) as kiekis
                  FROM sirgaliai
                  LEFT JOIN bilietai ON sirgaliai.id = bilietai.fk_SIRGALIUS
                  GROUP BY sirgaliai.id";

        $data = mysql::select($query);

        return $data;
    }

    public function getListCount()
    {
        $query = "  SELECT COUNT(`id`) as `kiekis`
					FROM {$this->fan_table}";
        $data = mysql::select($query);

        return $data[0]['kiekis'];
    }

    public function insert($data)
    {
        if($data['fk_ADRESAS'] >= 0) {
            $query = "  INSERT INTO {$this->fan_table}
                                        (
                                            `vardas`,
                                            `pavarde`,
                                            `telefono_numeris`,
                                            `elektroninis_pastas`,
                                            `gimimo_data`,
                                            `fk_ADRESAS`
                                        )
                                        VALUES
                                        (
                                            '{$data['vardas']}',
                                            '{$data['pavarde']}',
                                            '{$data['telefono_numeris']}',
                                            '{$data['elektroninis_pastas']}',
                                            '{$data['gimimo_data']}',
                                            '{$data['fk_ADRESAS']}'
                                        )";
            mysql::query($query);
        } else {
            $query = "  INSERT INTO {$this->fan_table}
                                        (
                                            `vardas`,
                                            `pavarde`,
                                            `telefono_numeris`,
                                            `elektroninis_pastas`,
                                            `gimimo_data`
                                        )
                                        VALUES
                                        (
                                            '{$data['vardas']}',
                                            '{$data['pavarde']}',
                                            '{$data['telefono_numeris']}',
                                            '{$data['elektroninis_pastas']}',
                                            '{$data['gimimo_data']}'
                                        )";
            mysql::query($query);
        }

        return mysql::getLastInsertedId();
    }

    public function getTickets($Id) {
        $query = "  SELECT *
					FROM `{$this->ticket_table}`
					WHERE `fk_SIRGALIUS`='{$Id}'";
        $data = mysql::select($query);

        return $data;
    }

    public function insertTickets($data) {
        if(isset($data['datos']) && sizeof($data['datos']) > 0) {
            foreach($data['datos'] as $key=>$val) {
                    $query = "  INSERT INTO `{$this->ticket_table}`
											(
											    `data`,
											    `kaina`,
											    `iejimas`,
											    `blokas`,
											    `eile`,
											    `vieta`,
											    `fk_SIRGALIUS`
											)
											VALUES
											(
											    '{$val}',
											    '{$data['kainos'][$key]}',
											    '{$data['iejimai'][$key]}',
											    '{$data['blokai'][$key]}',
											    '{$data['eiles'][$key]}',
											    '{$data['vietos'][$key]}',
												'{$data['id']}'
											)";
                    mysql::query($query);
            }
        }
    }

    public function deleteTickets($Id) {
        $query = "  DELETE FROM `{$this->ticket_table}`
					WHERE `fk_SIRGALIUS`='{$Id}'";
        mysql::query($query);
    }

    public function update($data)
    {
        $query = "  UPDATE {$this->fan_table}
					SET    `vardas`='{$data['vardas']}',
					       `pavarde`='{$data['pavarde']}',
					       `telefono_numeris`='{$data['telefono_numeris']}',
					       `elektroninis_pastas`='{$data['elektroninis_pastas']}',
					       `gimimo_data`='{$data['gimimo_data']}',
					       `fk_ADRESAS`='{$data['fk_ADRESAS']}'
					WHERE `id`='{$data['id']}'";
        mysql::query($query);
    }

    public function delete($id)
    {
        $query = "  DELETE FROM {$this->fan_table}
					WHERE `id`='{$id}'";
        mysql::query($query);
    }

    public function isUsedInOtherTableAsForeignKey($id) {
        $query = "  SELECT *
					FROM {$this->fan_match_table}
					WHERE {$this->fan_match_table}.`fk_SIRGALIUS`='{$id}'";
        $data = mysql::select($query);

        if($data != null)
            return true;

        return false;
    }
}