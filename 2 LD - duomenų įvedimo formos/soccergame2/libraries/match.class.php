<?php

class matches {

    private $match_table = '';
    private $team_table = '';
    private $referee_table = '';
    private $fan_table = '';
    private $fan_match_table = '';

    public function __construct() {
        $this->match_table = 'varzybos';
        $this->team_table = 'komandos';
        $this->referee_table = 'teisejai';
        $this->fan_table = 'sirgaliai';
        $this->fan_match_table = 'sirgaliai_varzybos';
    }

    public function get($id) {
        $query = "  SELECT *
					FROM {$this->match_table}
					WHERE `id`='{$id}'";
        $data = mysql::select($query);

        return $data[0];
    }

    public function getList($limit = null, $offset = null) {
        $limitOffsetString = "";
        if(isset($limit)) {
            $limitOffsetString .= " LIMIT {$limit}";

            if(isset($offset)) {
                $limitOffsetString .= " OFFSET {$offset}";
            }
        }

        $query = "  SELECT varzybos.`id`,
                           varzybos.`data`,
                           first.`pavadinimas` as 1komanda,
                           varzybos.`rezultatas`,
                           second.`pavadinimas` as 2komanda, 
                           CONCAT(referee.`vardas`, ' ', referee.`pavarde`) as teisejas
					FROM {$this->match_table} as varzybos
					INNER JOIN {$this->team_table} first ON {$this->match_table}.`fk_KOMANDA` = first.`id`
					INNER JOIN {$this->team_table} second ON {$this->match_table}.`fk_KOMANDA1` = second.`id`
					INNER JOIN {$this->referee_table} referee ON {$this->match_table}.`fk_TEISEJAS` = referee.`id`" . $limitOffsetString;
        $data = mysql::select($query);

        return $data;
    }

    public function getListCount() {
        $query = "  SELECT COUNT(`id`) as `kiekis`
					FROM {$this->match_table}";
        $data = mysql::select($query);

        return $data[0]['kiekis'];
    }

    public function insert($data) {
        $query = "  INSERT INTO {$this->match_table}
								(
									`data`,
									`sirgaliu_kiekis`,
									`rezultatas`,
									`fk_KOMANDA`,
									`fk_TEISEJAS`,
									`fk_KOMANDA1`
								)
								VALUES
								(
									'{$data['data']}',
									'{$data['sirgaliu_kiekis']}',
									'{$data['rezultatas']}',
									'{$data['fk_KOMANDA']}',
									'{$data['fk_TEISEJAS']}',
									'{$data['fk_KOMANDA1']}'
								)";
        mysql::query($query);
    }

    public function update($data) {
        $query = "  UPDATE {$this->match_table}
					SET    `data`='{$data['data']}',
					       `sirgaliu_kiekis`='{$data['sirgaliu_kiekis']}',
					       `rezultatas`='{$data['rezultatas']}',
					       `fk_KOMANDA`='{$data['fk_KOMANDA']}',
					       `fk_TEISEJAS`='{$data['fk_TEISEJAS']}',
					       `fk_KOMANDA1`='{$data['fk_KOMANDA1']}'
					WHERE `id`='{$data['id']}'";
        mysql::query($query);
    }

    public function delete($id) {
        $query = "  DELETE FROM {$this->match_table}
					WHERE `id`='{$id}'";
        mysql::query($query);
    }

    public function doesMatchHaveFans($id) {
        $query = "  SELECT * FROM {$this->fan_table}, {$this->fan_match_table}, {$this->match_table}
		            WHERE {$this->fan_table}.`id` = {$this->fan_match_table}.`fk_SIRGALIUS` 
		            AND {$this->match_table}.`id` = {$this->fan_match_table}.`fk_VARZYBOS`";
        $data = mysql::select($query);

        if($data != null)
            return true;

        return false;
    }
}