<?php

class address {

    private $address_table = '';
    private $fan_table = '';
    private $stadium_table = '';

    public function __construct() {
        $this->address_table = 'adresai';
        $this->fan_table = 'sirgaliai';
        $this->stadium_table = 'stadionai';
    }

    public function get($id) {
        $query = "  SELECT *
					FROM {$this->address_table}
					WHERE `id`='{$id}'";
        $data = mysql::select($query);

        return $data[0];
    }

    public function getList($limit = null, $offset = null) {
        $limitOffsetString = "";
        if(isset($limit)) {
            $limitOffsetString .= " LIMIT {$limit}";

            if(isset($offset)) {
                $limitOffsetString .= " OFFSET {$offset}";
            }
        }

        $query = "  SELECT *
					FROM {$this->address_table}{$limitOffsetString}";
        $data = mysql::select($query);

        return $data;
    }

    public function getListCount() {
        $query = "  SELECT COUNT(`id`) as `kiekis`
					FROM {$this->address_table}";
        $data = mysql::select($query);

        return $data[0]['kiekis'];
    }

    public function insert($data) {
        $query = "  INSERT INTO {$this->address_table}
								(
									`salis`,
									`miestas`,
									`gatve`,
									`pastato_nr`,
									`zip_kodas`
								)
								VALUES
								(
									'{$data['salis']}',
									'{$data['miestas']}',
									'{$data['gatve']}',
									'{$data['pastato_nr']}',
									'{$data['zip_kodas']}'
								)";
        mysql::query($query);
    }

    public function update($data) {
        $query = "  UPDATE {$this->address_table}
					SET    `salis`='{$data['salis']}',
					       `miestas`='{$data['miestas']}',
					       `gatve`='{$data['gatve']}',
					       `pastato_nr`='{$data['pastato_nr']}',
					       `zip_kodas`='{$data['zip_kodas']}'
					WHERE `id`='{$data['id']}'";
        mysql::query($query);
    }

    public function delete($id) {
        $query = "  DELETE FROM {$this->address_table}
					WHERE `id`='{$id}'";
        mysql::query($query);
    }

    public function doesAddressIsUsed($id) {
        $query = "  SELECT *
					FROM {$this->fan_table}
					WHERE {$this->fan_table}.`fk_ADRESAS`='{$id}'";
        $data = mysql::select($query);

        if($data != null)
            return true;

        $query = "  SELECT *
					FROM {$this->stadium_table}
					WHERE {$this->stadium_table}.`fk_ADRESAS`='{$id}'";
        $data = mysql::select($query);

        if($data != null)
            return true;

        return false;
    }
}