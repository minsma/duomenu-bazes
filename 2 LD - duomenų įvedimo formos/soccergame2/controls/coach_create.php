<?php

include 'libraries/coach.class.php';
$coaches = new coaches();

$formErrors = null;
$data = array();

$required = array(
    'vardas',
    'pavarde',
    'gimimo_data',
    'tautybe',
    'ugis',
    'atlyginimas',
    'kontrakto_pabaiga',
    'rinkos_verte'
);

$maxLengths = array (
	'vardas' => 20,
    'pavarde' => 20
);

if(!empty($_POST['submit'])) {
	$validations = array (
        'vardas' => 'alfanum',
        'pavarde' => 'alfanum',
        'gimimo_data' => 'date',
        'tautybe' => 'alfanum',
        'ugis' => 'positivenumber',
        'atlyginimas' => 'positivenumber',
        'kontrakto_pabaiga' => 'date',
        'rinkos_verte' => 'positivenumber'
    );

	include 'utils/validator.class.php';
	$validator = new validator($validations, $required, $maxLengths);

	if($validator->validate($_POST)) {
		$dataPrepared = $validator->preparePostFieldsForSQL();

        $coaches->insert($dataPrepared);

		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		$formErrors = $validator->getErrorHTML();
		$data = $_POST;
	}
}

include 'templates/coach_form.tpl.php';

?>