<?php

include 'libraries/coach.class.php';
$coaches = new coaches();

if(!empty($id)) {
	$doesHaveTeam = $coaches->doesCoachHaveTeam($id);

	$removeErrorParameter = '';
	if($doesHaveTeam == false) {
        $coaches->delete($id);
	} else {
		$removeErrorParameter = '&remove_error=1';
	}

	header("Location: index.php?module={$module}&action=list{$removeErrorParameter}");
	die();
}

?>