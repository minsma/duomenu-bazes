<?php

include 'libraries/referee.class.php';
$referees = new referees();

$elementCount = $referees->getListCount();

include 'utils/paging.class.php';
$paging = new paging(config::NUMBER_OF_ROWS_IN_PAGE);

$paging->process($elementCount, $pageId);

$data = $referees->getList($paging->size, $paging->first);

include 'templates/referee_list.tpl.php';

?>