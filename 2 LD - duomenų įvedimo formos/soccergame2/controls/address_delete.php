<?php

include 'libraries/address.class.php';
$address = new address();

if(!empty($id)) {
	$isUsed = $address->doesAddressIsUsed($id);

	$removeErrorParameter = '';
	if($isUsed == false) {
        $address->delete($id);
	} else {
		$removeErrorParameter = '&remove_error=1';
	}

	header("Location: index.php?module={$module}&action=list{$removeErrorParameter}");
	die();
}

?>