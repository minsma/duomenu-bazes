<?php

include 'libraries/fan.class.php';
$fans = new fans();

include 'libraries/address.class.php';
$address = new address();

$formErrors = null;
$data = array();

$required = array(
    'vardas',
    'pavarde',
    'telefono_numeris',
    'elektroninis_pastas',
    'gimimo_data',
    'datos',
    'kainos',
    'iejimai',
    'blokai',
    'eiles',
    'vietos'
);

$maxLengths = array (
    'vardas' => 20,
    'pavarde' => 20
);

if(!empty($_POST['submit'])) {
	$validations = array (
        'vardas' => 'alfanum',
        'pavarde' => 'alfanum',
        'telefono_numeris' => 'phone',
        'elektroninis_pastas' => 'email',
        'gimimo_data' => 'date',
        'datos' => 'date',
        'kainos' => 'price',
        'iejimai' => 'anything',
        'blokai' => 'anything',
        'eiles' => 'anything',
        'vietos' => 'int'
    );

	include 'utils/validator.class.php';
	$validator = new validator($validations, $required, $maxLengths);

	if($validator->validate($_POST)) {
		$dataPrepared = $validator->preparePostFieldsForSQL();

        $fans->update($dataPrepared);
        $fans->deleteTickets($dataPrepared['id']);
        $fans->insertTickets($dataPrepared);

		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		$formErrors = $validator->getErrorHTML();
		$data = $_POST;
	}
} else {
    if(!empty($id)) {
        $data = $fans->get($id);
        $tmp = $fans->getTickets($id);

        if(sizeof($tmp) > 0) {
            foreach($tmp as $key => $val) {
                $data['bilietai'][] = $val;
            }
        }
    }
}

include 'templates/fan_form.tpl.php';

?>