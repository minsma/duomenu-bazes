<?php

include 'libraries/team.class.php';
$teams = new teams();

if(!empty($id)) {
    $have = $teams->doesTeamHaveMatches($id);

    $removeErrorParameter = '';
    if($have == false) {
        $teams->delete($id);
    } else {
        $removeErrorParameter = '&remove_error=1';
    }

    header("Location: index.php?module={$module}&action=list{$removeErrorParameter}");
    die();
}

?>