<?php

include 'libraries/fan.class.php';
$fans = new fans();

if(!empty($id)) {
    $isUsed = $fans->isUsedInOtherTableAsForeignKey($id);

    $removeErrorParameter = '';
	if($isUsed == false){
	    $fans->deleteTickets($id);

        $fans->delete($id);
	} else {
		$removeErrorParameter = '&remove_error=1';
	}

	header("Location: index.php?module={$module}&action=list{$removeErrorParameter}");
	die();
}

?>