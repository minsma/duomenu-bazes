<?php

include 'libraries/match.class.php';
$matches = new matches();

if(!empty($id)) {
	$have = $matches->doesMatchHaveFans($id);

	$removeErrorParameter = '';
	if($have == false) {
        $matches->delete($id);
	} else {
		$removeErrorParameter = '&remove_error=1';
	}

	header("Location: index.php?module={$module}&action=list{$removeErrorParameter}");
	die();
}

?>