<?php

include 'libraries/team.class.php';
$teams = new teams();

$elementCount = $teams->getListCount();

include 'utils/paging.class.php';
$paging = new paging(config::NUMBER_OF_ROWS_IN_PAGE);

$paging->process($elementCount, $pageId);

$data = $teams->getList($paging->size, $paging->first);

include 'templates/team_list.tpl.php';

?>