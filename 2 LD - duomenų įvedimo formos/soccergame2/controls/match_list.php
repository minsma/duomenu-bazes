<?php

include 'libraries/match.class.php';
$matches = new matches();

$elementCount = $matches->getListCount();

include 'utils/paging.class.php';
$paging = new paging(config::NUMBER_OF_ROWS_IN_PAGE);

$paging->process($elementCount, $pageId);

$data = $matches->getList($paging->size, $paging->first);

include 'templates/match_list.tpl.php';

?>