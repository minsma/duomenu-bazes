<?php

include 'libraries/team.class.php';
$teams = new teams();

include 'libraries/coach.class.php';
$coaches = new coaches();

$formErrors = null;
$data = array();

$required = array(
    'pavadinimas',
    'salis',
    'miestas',
    'ikurimo_data',
    'svetaine',
    'biudzetas'
);

if(!empty($_POST['submit'])) {
    $validations = array (
        'pavadinimas' => 'alfanum',
        'salis' => 'alfanum',
        'miestas' => 'alfanum',
        'ikurimo_data' => 'date',
        'svetaine' => 'alfanum',
        'biudzetas' => 'positivenumber'
    );

    include 'utils/validator.class.php';
    $validator = new validator($validations, $required);

    if($validator->validate($_POST)) {
        $dataPrepared = $validator->preparePostFieldsForSQL();

        $teams->update($dataPrepared);

        header("Location: index.php?module={$module}&action=list");
        die();
    } else {
        $formErrors = $validator->getErrorHTML();
        $data = $_POST;
    }
} else {
    $data = $teams->get($id);
}

include 'templates/team_form.tpl.php';

?>