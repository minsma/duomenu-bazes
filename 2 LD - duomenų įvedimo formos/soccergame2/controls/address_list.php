<?php

include 'libraries/address.class.php';
$address = new address();

$elementCount = $address->getListCount();

include 'utils/paging.class.php';
$paging = new paging(config::NUMBER_OF_ROWS_IN_PAGE);

$paging->process($elementCount, $pageId);

$data = $address->getList($paging->size, $paging->first);

include 'templates/address_list.tpl.php';

?>