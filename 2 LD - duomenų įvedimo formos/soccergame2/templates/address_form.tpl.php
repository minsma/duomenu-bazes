<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>&action=list">Adresai</a></li>
	<li><?php if(!empty($id)) echo "Adreso duomenų redagavimas"; else echo "Pridėti adresą"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>

        'salis',
        'miestas',
        'gatve',
        'pastato_nr',
        'zip_kodas'
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Trenerio informacija</legend>
			<p>
				<label class="field" for="salis">Šalis<?php echo in_array('salis', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="salis" name="salis" class="textbox textbox-150" value="<?php echo isset($data['salis']) ? $data['salis'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="miestas">Miestas<?php echo in_array('miestas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="miestas" name="miestas" class="textbox textbox-150" value="<?php echo isset($data['miestas']) ? $data['miestas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="gatve">Gatvė<?php echo in_array('gatve', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="gatve" name="gatve" class="textbox textbox-150" value="<?php echo isset($data['gatve']) ? $data['gatve'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="pastato_nr">Pastato numeris<?php echo in_array('pastato_nr', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="pastato_nr" name="pastato_nr" class="textbox textbox-150" value="<?php echo isset($data['pastato_nr']) ? $data['pastato_nr'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="zip_kodas">Pašto kodas<?php echo in_array('zip_kodas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="zip_kodas" name="zip_kodas" class="textbox textbox-150" value="<?php echo isset($data['zip_kodas']) ? $data['zip_kodas'] : ''; ?>">
            </p>
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit button" name="submit" value="Išsaugoti">
		</p>
		<?php if(isset($data['id'])) { ?>
			<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
		<?php } ?>
	</form>
</div>