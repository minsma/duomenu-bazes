<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Adresai</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=create'>Pridėti adresą</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Adresas nebuvo pašalintas.
	</div>
<?php } ?>

<table class="listTable">
	<tr>
		<th>ID</th>
		<th>Šalis</th>
        <th>Miestas</th>
        <th>Gatvė</th>
        <th>Pastato numeris</th>
        <th>Pašto kodas</th>
		<th></th>
	</tr>
	<?php
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id']}</td>"
					. "<td>{$val['salis']}</td>"
                    . "<td>{$val['miestas']}</td>"
                    . "<td>{$val['gatve']}</td>"
                    . "<td>{$val['pastato_nr']}</td>"
                    . "<td>{$val['zip_kodas']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&action=edit&id={$val['id']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	include 'templates/paging.tpl.php';
?>