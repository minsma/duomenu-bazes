<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Komandos</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=create'>Pridėti komandą</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Komanda nebuvo pašalinta.
	</div>
<?php } ?>
<table class="listTable">
	<tr>
		<th>ID</th>
		<th>Pavadinimas</th>
        <th>Šalis</th>
        <th>Miestas</th>
        <th>Įkūrimo data</th>
        <th>Svetainė</th>
        <th>Biudžetas</th>
        <th>Treneris</th>
		<th></th>
	</tr>
	<?php
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id']}</td>"
					. "<td>{$val['pavadinimas']}</td>"
                    . "<td>{$val['salis']}</td>"
                    . "<td>{$val['miestas']}</td>"
                    . "<td>{$val['ikurimo_data']}</td>"
                    . "<td>{$val['svetaine']}</td>"
                    . "<td>{$val['biudzetas']}</td>"
                    . "<td>{$val['treneris']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&action=edit&id={$val['id']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	include 'templates/paging.tpl.php';
?>