<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Sirgaliai</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=create'>Pridėti sirgalių</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Sirgalius nebuvo pašalintas.
	</div>
<?php } ?>

<table class="listTable">
	<tr>
		<th>ID</th>
		<th>Vardas</th>
        <th>Pavardė</th>
        <th>Telefono Numeris</th>
        <th>Elektroninis Paštas</th>
        <th>Gimimo Data</th>
        <th>Kiekis</th>
		<th></th>
	</tr>
	<?php
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id']}</td>"
					. "<td>{$val['vardas']}</td>"
                    . "<td>{$val['pavarde']}</td>"
                    . "<td>{$val['telefono_numeris']}</td>"
                    . "<td>{$val['elektroninis_pastas']}</td>"
                    . "<td>{$val['gimimo_data']}</td>"
                    . "<td>{$val['kiekis']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&action=edit&id={$val['id']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	include 'templates/paging.tpl.php';
?>