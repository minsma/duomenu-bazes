<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>&action=list">Komandos</a></li>
	<li><?php if(!empty($id)) echo "Komandos duomenų redagavimas"; else echo "Pridėti komandą"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Komandos informacija</legend>
			<p>
				<label class="field" for="pavadinimas">Pavadinimas<?php echo in_array('pavadinimas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pavadinimas" name="pavadinimas" class="textbox textbox-150" value="<?php echo isset($data['pavadinimas']) ? $data['pavadinimas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="salis">Šalis<?php echo in_array('salis', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="salis" name="salis" class="textbox textbox-150" value="<?php echo isset($data['salis']) ? $data['salis'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="miestas">Miestas<?php echo in_array('miestas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="miestas" name="miestas" class="textbox textbox-150" value="<?php echo isset($data['miestas']) ? $data['miestas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="ikurimo_data">Įkūrimo data<?php echo in_array('ikurimo_data', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="ikurimo_data" name="ikurimo_data" class="textbox textbox-150 date" value="<?php echo isset($data['ikurimo_data']) ? $data['ikurimo_data'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="svetaine">Svetainė<?php echo in_array('svetaine', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="text" id="svetaine" name="svetaine" class="textbox textbox-150" value="<?php echo isset($data['svetaine']) ? $data['svetaine'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="biudzetas">Biudžetas<?php echo in_array('biudzetas', $required) ? '<span> *</span>' : ''; ?></label>
                <input type="number" id="biudzetas" name="biudzetas" class="textbox textbox-150" value="<?php echo isset($data['biudzetas']) ? $data['biudzetas'] : ''; ?>">
            </p>
            <p>
                <label class="field" for="treneris">Treneris<?php echo in_array('treneris', $required) ? '<span> *</span>' : ''; ?></label>
                <select id="team" name="fk_TRENERIS">
                    <option value="-1">Pasirinkite trenerį</option>
                    <?php
                    //$coach = $coaches->getListCoachesWithoutTeam();

                    $coach = $coaches->getList();
                    foreach($coach as $key => $val) {
                        $selected = "";
                        if(isset($data['fk_TRENERIS']) && $data['fk_TRENERIS'] == $val['id']) {
                            $selected = " selected='selected'";
                        }
                        echo "<option{$selected} value='{$val['id']}'>{$val['vardas']} {$val['pavarde']}</option>";
                    }
                    ?>
                </select>
			</p>
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit button" name="submit" value="Išsaugoti">
		</p>
		<?php if(isset($data['id'])) { ?>
			<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
		<?php } ?>
	</form>
</div>