<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="robots" content="noindex">
		<title>Futbolo Varžybų IS</title>
		<link rel="stylesheet" type="text/css" href="scripts/datetimepicker/jquery.datetimepicker.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="style/main.css" media="screen" />
		<script type="text/javascript" src="scripts/jquery-1.12.0.min.js"></script>
		<script type="text/javascript" src="scripts/datetimepicker/jquery.datetimepicker.full.min.js"></script>
		<script type="text/javascript" src="scripts/main.js"></script>
	</head>
	<body>
		<div id="body">
			<div id="header">
				<h3 id="slogan"><a href="index.php">Futbolo Varžybų IS</a></h3>
			</div>
			<div id="content">
				<div id="topMenu">
					<ul class="float-left">
						<li><a href="index.php?module=coach&action=list" title="Treneriai"<?php if($module == 'coach') { echo 'class="active"'; } ?>>Treneriai</a></li>
                        <li><a href="index.php?module=team&action=list" title="Komandos"<?php if($module == 'team') { echo 'class="active"'; } ?>>Komandos</a></li>
                        <li><a href="index.php?module=fan&action=list" title="Sirgaliai"<?php if($module == 'fan') { echo 'class="active"'; } ?>>Sirgaliai</a></li>
                        <li><a href="index.php?module=match&action=list" title="Varžybos"<?php if($module == 'match') { echo 'class="active"'; } ?>>Varžybos</a></li>
                        <li><a href="index.php?module=referee&action=list" title="Teisėjai"<?php if($module == 'referee') { echo 'class="active"'; } ?>>Teisėjai</a></li>
                        <li><a href="index.php?module=address&action=list" title="Adresai"<?php if($module == 'address') { echo 'class="active"'; } ?>>Adresai</a></li>
                    </ul>
				</div>
				<div id="contentMain">
					<?php
						if(file_exists($actionFile)) {
							include $actionFile;
						}
					?>
					<div class="float-clear"></div>
				</div>
			</div>
			<div id="footer">

			</div>
		</div>
	</body>
</html>