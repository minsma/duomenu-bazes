<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Treneriai</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=create'>Pridėti trenerį</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Treneris nebuvo pašalintas.
	</div>
<?php } ?>

<table class="listTable">
	<tr>
		<th>ID</th>
		<th>Vardas</th>
        <th>Pavardė</th>
        <th>Gimimo data</th>
        <th>Tautybė</th>
        <th>Ūgis</th>
        <th>Atlyginimas</th>
        <th>Kontrakto pabaiga</th>
        <th>Rinkos vertė</th>
		<th></th>
	</tr>
	<?php
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id']}</td>"
					. "<td>{$val['vardas']}</td>"
                    . "<td>{$val['pavarde']}</td>"
                    . "<td>{$val['gimimo_data']}</td>"
                    . "<td>{$val['tautybe']}</td>"
                    . "<td>{$val['ugis']}</td>"
                    . "<td>{$val['atlyginimas']}</td>"
                    . "<td>{$val['kontrakto_pabaiga']}</td>"
                    . "<td>{$val['rinkos_verte']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&action=edit&id={$val['id']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	include 'templates/paging.tpl.php';
?>